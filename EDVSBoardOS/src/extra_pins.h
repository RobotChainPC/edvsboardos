/*
 * extra_pins.h
 *
 *  Created on: 5 de Ago de 2013
 *      Author: ruka
 */

#ifndef EXTRA_PINS_H_
#define EXTRA_PINS_H_
#include "chip.h"

// P1.17 VBAT reset signal
#define VBAT_GND_PORT_GPIO  		(0)
#define VBAT_GND_PIN_GPIO  			(12)
#define VBAT_GND_PORT  				(1)
#define VBAT_GND_PIN  				(17)

// P1.15 FTDI reset signal
#define FTDI_RESET_PORT_GPIO  		(0)
#define FTDI_RESET_PIN_GPIO  		(2)
#define FTDI_RESET_PORT  			(1)
#define FTDI_RESET_PIN  			(15)

/* The bit of port 0 that the LPCXpresso LPC43xx LED0 is connected. */
#define LED0_PORT_GPIO  			(0)
#define LED0_PIN_GPIO  				(0)
#define LED0_PORT  					(0)
#define LED0_PIN	 				(0)
/* The bit of port 0 that the LPCXpresso LPC43xx LED1 is connected. */
#define LED1_PORT_GPIO  			(0)
#define LED1_PIN_GPIO  				(1)
#define LED1_PORT  					(0)
#define LED1_PIN	 				(1)


void ExtraPinsInit();

static inline void LEDSetOff() {
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO); //Turn on the LED
}
static inline void LEDSetOn() {
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO); //Turn off the LED

}

#endif /* EXTRA_PINS_H_ */
