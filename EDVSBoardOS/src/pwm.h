/*
 * pwm.h
 *
 *  Created on: Apr 11, 2014
 *      Author: raraujo
 */

#ifndef PWM_H_
#define PWM_H_
#include <stdint.h>
extern void PWMInit(void);
extern void PWMSetFrequency(uint8_t channel, uint32_t frequency);
extern void PWMSetDutyCycle(uint8_t channel, uint8_t output, uint32_t dutycycle);

#endif /* PWM_H_ */
