/*
 * sensors.h
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#ifndef SENSORS_H_
#define SENSORS_H_
#include <stdint.h>

#define MAX_SENSORS							(32)
struct sensorTimer {
	volatile uint16_t triggered;
	uint16_t position;
	volatile uint32_t reload;
	volatile uint32_t counter;
	void (*init)(void);
	void (*refresh)(void);
};
extern volatile uint8_t sensorRefreshRequested;
extern struct sensorTimer sensorsTimers[MAX_SENSORS];
extern struct sensorTimer * enabledSensors[MAX_SENSORS];
extern uint32_t sensorsEnabledCounter;
extern void sensorsInit(void);
extern void enableSensors(uint32_t mask, uint8_t flag, uint32_t period);
extern void enableSensor(uint8_t sensorId, uint8_t flag, uint32_t period);

#endif /* SENSORS_H_ */
