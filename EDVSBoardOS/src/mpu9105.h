/*
 * mpu9105.h
 *
 *  Created on: Apr 7, 2014
 *      Author: raraujo
 */

#ifndef MPU9105_H_
#define MPU9105_H_
#include <stdint.h>

extern volatile int16_t gyrometer_data[3];
extern volatile int16_t temperature;
extern volatile int16_t accelerometer_data[3];
extern volatile int16_t magnometer_data[3];

extern void MPU9105Init(void);
extern void updateIMUData(void);
#endif /* MPU9105_H_ */
