/*
 * pwm.c
 *
 *  Created on: Apr 11, 2014
 *      Author: raraujo
 */
#include "pwm.h"
#include "chip.h"

#define MAX_OUTPUTS 2

#define PWM_RESOLUTION 100

#define CHANNEL_A_TIMER   		LPC_TIMER3
#define CHANNEL_A_TIMER_INDEX	(0)
#define CHANNEL_A_0_PORT		(2)
#define CHANNEL_A_0_PIN			(3)
#define CHANNEL_A_1_PORT		(2)
#define CHANNEL_A_1_PIN			(4)

#define CHANNEL_B_TIMER   		LPC_TIMER2
#define CHANNEL_B_TIMER_INDEX	(1)
#define CHANNEL_B_0_PORT		(6)
#define CHANNEL_B_0_PIN			(7)
#define CHANNEL_B_1_PORT		(6)
#define CHANNEL_B_1_PIN			(8)

#define CHANNEL_C_TIMER   		LPC_TIMER0
#define CHANNEL_C_TIMER_INDEX	(2)
#define CHANNEL_C_0_PORT		(2)
#define CHANNEL_C_0_PIN			(8)
#define CHANNEL_C_1_PORT		(2)
#define CHANNEL_C_1_PIN			(9)

void PWMInit(void) {
	Chip_SCU_PinMuxSet(CHANNEL_A_0_PORT, CHANNEL_A_0_PIN, MD_PUP | FUNC6);
	Chip_SCU_PinMuxSet(CHANNEL_A_1_PORT, CHANNEL_A_1_PIN, MD_PUP | FUNC6);
	Chip_SCU_PinMuxSet(CHANNEL_B_0_PORT, CHANNEL_B_0_PIN, MD_PUP | FUNC5);
	Chip_SCU_PinMuxSet(CHANNEL_B_1_PORT, CHANNEL_B_1_PIN, MD_PUP | FUNC5);
	Chip_SCU_PinMuxSet(CHANNEL_C_0_PORT, CHANNEL_C_0_PIN, MD_PUP | FUNC1);
	Chip_SCU_PinMuxSet(CHANNEL_C_1_PORT, CHANNEL_C_1_PIN, MD_PUP | FUNC1);
}

void PWMSetFrequency(uint8_t channel, uint32_t frequency) {
	if (frequency > Chip_Clock_GetRate(CLK_MX_MXCORE)) {
		return;
	}
	LPC_TIMER_T * timer;
	uint32_t clock = 0;
	switch (channel) {
	case CHANNEL_A_TIMER_INDEX:
		timer = CHANNEL_A_TIMER;
		clock = Chip_Clock_GetRate(CLK_MX_TIMER3);
		break;
	case CHANNEL_B_TIMER_INDEX:
		timer = CHANNEL_B_TIMER;
		clock = Chip_Clock_GetRate(CLK_MX_TIMER2);
		break;
	case CHANNEL_C_TIMER_INDEX:
		timer = CHANNEL_C_TIMER;
		clock = Chip_Clock_GetRate(CLK_MX_TIMER0);
		break;
	default:
		return;
	}

	if (frequency == 0) {
		Chip_TIMER_DeInit(timer); //Stop the timer
	} else {
		Chip_TIMER_Init(timer);
		Chip_TIMER_Disable(timer);
		Chip_TIMER_Reset(timer);
		Chip_TIMER_ResetOnMatchEnable(timer, 2);
		Chip_TIMER_StopOnMatchDisable(timer, 2);
		Chip_TIMER_MatchDisableInt(timer, 2);
		Chip_TIMER_SetMatch(timer, 2, (clock >> 1) / frequency);
		// Clear interrupt pending
		timer->IR = 0xFFFFFFFF;
		Chip_TIMER_Enable(timer); //Enable timer
	}
}
/*
 * Dutycycle should be between 0 and PWM_RESOLUTION
 */
void PWMSetDutyCycle(uint8_t channel, uint8_t output, uint32_t dutycycle) {
	if (output >= MAX_OUTPUTS) {
		return;
	}
	LPC_TIMER_T * timer;
	switch (channel) {
	case CHANNEL_A_TIMER_INDEX:
		timer = CHANNEL_A_TIMER;
		break;
	case CHANNEL_B_TIMER_INDEX:
		timer = CHANNEL_B_TIMER;
		break;
	case CHANNEL_C_TIMER_INDEX:
		timer = CHANNEL_C_TIMER;
		break;
	default:
		return;
	}
	if (channel == CHANNEL_C_TIMER_INDEX && output == 1) {
		output = 3; // Special mapping for the Channel C_1
	}
	Chip_TIMER_ExtMatchControlSet(timer, 1, TIMER_EXTMATCH_TOGGLE, output);
	Chip_TIMER_ResetOnMatchDisable(timer, output);
	Chip_TIMER_StopOnMatchDisable(timer, output);
	Chip_TIMER_MatchDisableInt(timer, output);
	Chip_TIMER_SetMatch(timer, output, 0);
}
