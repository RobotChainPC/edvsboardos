/*
 * dispatcher.c
 *
 *  Created on: 5 de Dez de 2012
 *      Author: ruka
 */
#include <stdlib.h>
#include "uart.h"
#include "EDVS128_LPC43xx.h"
#include "motors.h"
#include "sleep.h"

#define IDLE 0
#define RECEIVING_MOTOR_NUMBER 1
#define RECEIVING_MOTOR_SPEED 2
#define RECEIVING_BIAS 3
#define RECEIVING_TIME_DATE 4
#define RECEIVING_DAC_VALUE 5

#define SLEEP_COMMAND 's'
#define BIAS_COMMAND 'b'
#define MOTOR_COMMAND 'm'
#define DRIVER_COMMAND 'd'
#define CLOCK_COMMAND 'c'
#define RECORD_COMMAND 'r'
#define DAC_COMMAND 'p'
#define MOTOR_COMMAND_SEP ','
#define COMMAND_END '|'

//length of "2012-03-02 12:00:00"
#define TIME_DATE_COM_SIZE (20)

//static RTC_TIME_Type new_time;
static uint8_t state = IDLE;
static int32_t speed;
static int32_t direction;
static char buffer[20] = { 0 };
static int buffer_pos = 0;

void parseIncomingChar(uint8_t ulReceivedValue) {
	if (state == IDLE) {
		switch (ulReceivedValue) {
		case MOTOR_COMMAND:
			state = RECEIVING_MOTOR_NUMBER;
			break;
		case BIAS_COMMAND:
			state = RECEIVING_BIAS;
			break;
		case SLEEP_COMMAND:
			sleep();
			break;
		case RECORD_COMMAND:
			shouldRecord = !shouldRecord;
			break;
		case CLOCK_COMMAND:
			state = RECEIVING_TIME_DATE;
			break;
		case DAC_COMMAND:
			state = RECEIVING_DAC_VALUE;
			break;
		default:
			break;
		}
	} else if (state == RECEIVING_MOTOR_NUMBER) {
		if (ulReceivedValue == MOTOR_COMMAND_SEP) {
			state = RECEIVING_MOTOR_SPEED;
			buffer[buffer_pos] = '\0';
			direction = atoi(buffer);
			buffer_pos = 0;
		} else
			buffer[buffer_pos++] = ulReceivedValue;
	} else if (state == RECEIVING_MOTOR_SPEED) {
		if (ulReceivedValue == COMMAND_END) {
			state = IDLE;
			buffer[buffer_pos] = '\0';
			speed = atoi(buffer);
			buffer_pos = 0;
			updateMotorDutyCycle(direction, speed);
		} else
			buffer[buffer_pos++] = ulReceivedValue;
	} else if (state == RECEIVING_BIAS) {
		if (ulReceivedValue >= '0' || ulReceivedValue <= '5') {
			DVS128BiasLoadDefaultSet(ulReceivedValue - '0');
			DVS128BiasFlush(1);
		}
		state = IDLE;
	} else if (state == RECEIVING_TIME_DATE) {
		buffer[buffer_pos++] = ulReceivedValue;
		if (buffer_pos == TIME_DATE_COM_SIZE - 1) {
#if 0

			buffer[4] = '\0'; //year
			new_time.YEAR = atoi(buffer);
			buffer[7] = '\0'; //month
			new_time.MONTH = atoi(buffer + 5);
			buffer[10] = '\0'; //day
			new_time.DOM = atoi(buffer + 8);
			buffer[13] = '\0'; //hours
			new_time.HOUR = atoi(buffer + 11);
			buffer[16] = '\0'; //minutes
			new_time.MIN = atoi(buffer + 14);
			buffer[buffer_pos] = '\0'; //seconds
			new_time.SEC = atoi(buffer + 17);
			new_time.DOW = new_time.DOY = 0;
			RTC_SetFullTime(LPC_RTC, &new_time);
			state = IDLE;
			buffer_pos = 0;
#endif
		}
	} else if (state == RECEIVING_DAC_VALUE) {
		if (ulReceivedValue == COMMAND_END) {
			state = IDLE;
			buffer[buffer_pos] = '\0';
			speed = atoi(buffer);
			buffer_pos = 0;
			DAC_UpdateValue(LPC_DAC, speed);
		} else
			buffer[buffer_pos++] = ulReceivedValue;
	}

}
