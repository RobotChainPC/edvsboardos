/*
 * sleep.h
 *
 *  Created on: Mar 9, 2013
 *      Author: raraujo
 */

#ifndef SLEEP_H_
#define SLEEP_H_

#include <stdint.h>

void timer_delay_us(uint32_t us);
void timer_delay_ms(uint32_t ms);

void sleep();
void DacInit();
void disablePeripherals();
void resetDevice();
void enterReprogrammingMode();
#endif /* SLEEP_H_ */
