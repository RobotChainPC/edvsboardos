#ifndef DVS_H_
#define DVS_H_

#include <stdio.h>
#include <stdint.h>

// ***************************************************************************** event time stamp resolution
//#define TIMESTAMP_SHIFTBITS					(5)		// 192MHz/3 >> 5 = 2MHz   --> 0.5us   resolution
#define TIMESTAMP_SHIFTBITS					(6)		// 192MHz/3 >> 6 = 1MHz   --> 1us   resolution//#define TIMESTAMP_SHIFTBITS					(7)		// 192MHz/3 >> 7 = 0.5MHz   --> 2us   resolution
// ***************************************************************************** data formats
#define EDVS_DATA_FORMAT_DEFAULT			EDVS_DATA_FORMAT_BIN

#define EDVS_DATA_FORMAT_BIN				 0					//  2 Bytes/event#define EDVS_DATA_FORMAT_BIN_TS2B			 1					//  4 Bytes/event#define EDVS_DATA_FORMAT_BIN_TS3B			 2					//  5 Bytes/event// *****************************************************************************
#define DVS_EVENTBUFFER_SIZE_BITS		((uint32_t) 12)
#define DVS_EVENTBUFFER_SIZE		  	(((uint32_t) 1)<<DVS_EVENTBUFFER_SIZE_BITS)
#define DVS_EVENTBUFFER_MASK		  	(DVS_EVENTBUFFER_SIZE - 1)

// *************************************** Pinout definitions

// P3.4 signal to bias clock GPIO1[14]
#define PORT_EVENT_Y				(7)
#define PORT_EVENT_X				(6)
#define PIN_EVENT_X0				(1)
#define PIN_EVENT_X1				(2)
#define PIN_EVENT_X2				(3)
#define PIN_EVENT_X3				(4)
#define PIN_EVENT_X4				(5)
#define PIN_EVENT_X5				(9)
#define PIN_EVENT_X6				(10)
#define PIN_EVENT_P					(11)
#define PIN_EVENT_Y0				(0)
#define PIN_EVENT_Y1				(1)
#define PIN_EVENT_Y2				(2)
#define PIN_EVENT_Y3				(3)
#define PIN_EVENT_Y4				(4)
#define PIN_EVENT_Y5				(5)
#define PIN_EVENT_Y6				(6)

// P3.5 signal to bias clock GPIO1[15]
#define PORT_BIAS_CLOCK				(3)
#define PIN_BIAS_CLOCK				(5)
#define GPIO_PORT_BIAS_CLOCK		(1)
#define GPIO_PIN_BIAS_CLOCK			(15)

// P3.7 signal to bias setup GPIO5[10]
#define PORT_BIAS_DATA				(3)
#define PIN_BIAS_DATA				(7)
#define GPIO_PORT_BIAS_DATA			(5)
#define GPIO_PIN_BIAS_DATA			(10)

// P2.12 signal to bias latch GPIO1[12]
#define PORT_BIAS_LATCH				(2)
#define PIN_BIAS_LATCH				(12)
#define GPIO_PORT_BIAS_LATCH		(1)
#define GPIO_PIN_BIAS_LATCH			(12)

// P2.11 reset DVS GPIO1[11]
#define PORT_RESET_DVS				(2)
#define PIN_RESET_DVS				(11)
#define GPIO_PORT_RESET_DVS			(1)
#define GPIO_PIN_RESET_DVS			(11)

// P1.6 DVS request (input to LPC) (Pin CAP)
#define PORT_DVS_REQUEST			(5)
#define PIN_DVS_REQUEST				(1)
#define GPIO_PORT_DVS_REQUEST		(2)
#define GPIO_PIN_DVS_REQUEST		(10)

// P1.8 DVS acknowledge (output to DVS) set as input
#define PORT_DVS_ACKN				(7)
#define PIN_DVS_ACKN				(7)
#define GPIO_PORT_DVS_ACKN			(3)
#define GPIO_PIN_DVS_ACKN			(15)

// P2.10 Output Enable ( Negative Logic) GPIO0[14]
#define PORT_DVS_OE					(2)
#define PIN_DVS_OE					(10)
#define GPIO_PORT_DVS_OE			(0)
#define GPIO_PIN_DVS_OE				(14)

// P4.0 Ground for Bias resistor
#define PORT_RX_DEFAULT_GND			(4)
#define PIN_RX_DEFAULT_GND			(0)
#define GPIO_PORT_RX_DEFAULT_GND	(2)
#define GPIO_PIN_RX_DEFAULT_GND		(0)

#define EVENT_PORT					(3)
#define PIN_ALL_ADDR            ((uint32_t) 0x00007FFF)// all 15 address bits from DVS#define PIN_ADDR_X              ((uint32_t) 0x0000007F)// address bits X#define PIN_ADDR_P              ((uint32_t) 0x00000080)// bit specifying polarity of event#define PIN_ADDR_Y              ((uint32_t) 0x00007F00)// address bits Y#define MEM_ADDR_X              ((uint32_t) 0x0000007F)// memory address bits X#define MEM_ADDR_P              ((uint32_t) 0x00000080)// memory bit specifying polarity of event#define MEM_ADDR_Y              ((uint32_t) 0x00007F00)// memory address bits Y#define OVERFLOW_BIT            ((uint32_t) 0x00008000)// this bit denotes overflow (in event memory, not on pin)// *************************************** bias definitions
#define BIAS_cas			 0
#define BIAS_injGnd			 1
#define BIAS_reqPd			 2
#define BIAS_puX			 3
#define BIAS_diffOff		 4
#define BIAS_req			 5
#define BIAS_refr			 6
#define BIAS_puY			 7
#define BIAS_diffOn			 8
#define BIAS_diff			 9
#define BIAS_foll			10
#define BIAS_Pr				11

//BIAS seetings

#define BIAS_DEFAULT				0			// "BIAS_DEFAULT"#define BIAS_BRAGFOST				1			// "BIAS_BRAGFOST"#define BIAS_FAST					2			// "BIAS_FAST"#define BIAS_STEREO_PAIR			3			// "BIAS_STEREO_PAIR"#define BIAS_MINI_DVS				4			// "BIAS_MINI_DVS"#define BIAS_BRAGFOST_BALANCED		5			// "BIAS_BRAGFOST - ON/OFF Balanced"#define TIMER_CAPTURE_CHANNEL 		(1)
// *************************************** Function prototypes

extern uint32_t eDVSDataFormat;
extern uint32_t shouldRecord;
extern uint32_t enableEventSending;

extern volatile uint32_t lastEventCount;
extern volatile uint32_t currentEventRate;
extern volatile uint16_t eventBufferA[DVS_EVENTBUFFER_SIZE];	// for event addresses
extern volatile uint32_t eventBufferT[DVS_EVENTBUFFER_SIZE];	// for time stamps
extern volatile uint32_t eventBufferWritePointer;
extern volatile uint32_t eventBufferReadPointer;

extern void RTCInit();
extern void getFilename(char * filename, uint32_t DVSEventTime);
extern void DVS128ChipInit(void);
extern uint32_t DVS128BiasGet(uint32_t biasID);
extern void DVS128BiasSet(uint32_t biasID, uint32_t biasValue);
extern void DVS128BiasLoadDefaultSet(uint32_t biasSetID);
extern void DVS128BiasFlush(uint32_t multiplier);
extern void DVS128FetchEventsEnable(unsigned char flag);

#endif
