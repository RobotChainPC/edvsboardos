/*
 * minirob.c
 *
 *  Created on: 13/04/2014
 *      Author: ruka
 */
#include "sensors.h"
#include "minirob.h"
#include "chip.h"
#include <stdio.h>

#define SENSOR_ID	24

#define RIGHT_SENSOR_A_POSITION		(0)
#define RIGHT_SENSOR_A_PORT			(4)
#define RIGHT_SENSOR_A_PIN			(6)
#define RIGHT_SENSOR_A_PORT_GPIO	(2)
#define RIGHT_SENSOR_A_PIN_GPIO		(6)

#define RIGHT_SENSOR_B_POSITION		(1)
#define RIGHT_SENSOR_B_PORT			(4)
#define RIGHT_SENSOR_B_PIN			(5)
#define RIGHT_SENSOR_B_PORT_GPIO	(2)
#define RIGHT_SENSOR_B_PIN_GPIO		(5)

#define LEFT_SENSOR_A_POSITION		(2)
#define LEFT_SENSOR_A_PORT			(4)
#define LEFT_SENSOR_A_PIN			(2)
#define LEFT_SENSOR_A_PORT_GPIO		(2)
#define LEFT_SENSOR_A_PIN_GPIO		(2)

#define LEFT_SENSOR_B_POSITION		(3)
#define LEFT_SENSOR_B_PORT			(4)
#define LEFT_SENSOR_B_PIN			(8)
#define LEFT_SENSOR_B_PORT_GPIO		(5)
#define LEFT_SENSOR_B_PIN_GPIO		(12)

struct {
	volatile uint8_t phaseA;
	volatile uint8_t phaseB;
	volatile uint16_t previousState;
	volatile int32_t wheelStatus;
	volatile uint32_t errorCount;
} left, right;

void refreshMiniRobSensors() {
	uint32_t currentState;
	left.phaseA = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LEFT_SENSOR_A_PORT_GPIO, LEFT_SENSOR_A_PIN_GPIO);
	left.phaseB = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LEFT_SENSOR_B_PORT_GPIO, LEFT_SENSOR_B_PIN_GPIO);
	right.phaseA = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, RIGHT_SENSOR_A_PORT_GPIO, RIGHT_SENSOR_A_PIN_GPIO);
	right.phaseB = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, RIGHT_SENSOR_B_PORT_GPIO, RIGHT_SENSOR_B_PIN_GPIO);

	if (left.phaseA) {
		currentState = left.phaseB ? 2 : 3;
	} else {
		currentState = left.phaseB ? 1 : 0;
	}
	if (currentState == ((left.previousState + 1) % 4)) {
		left.wheelStatus++;
	} else if (left.previousState == ((currentState + 1) % 4)) {
		left.wheelStatus--;
	} else if (currentState != left.previousState) {
		left.errorCount++;
	}
	left.previousState = currentState;

	if (right.phaseA) {
		currentState = right.phaseB ? 2 : 3;
	} else {
		currentState = right.phaseB ? 1 : 0;
	}
	if (currentState == ((right.previousState + 1) % 4)) {
		right.wheelStatus++;
	} else if (right.previousState == ((currentState + 1) % 4)) {
		right.wheelStatus--;
	} else if (currentState != right.previousState) {
		right.errorCount++;
	}
	right.previousState = currentState;
}

void reportValues() {
	printf("-S%d %d %d\n", SENSOR_ID, left.wheelStatus, right.wheelStatus);
}

void sensorInit() {
#if 0
	LPC_SCU->PINTSEL0 = RIGHT_SENSOR_A_PIN_GPIO | ( RIGHT_SENSOR_A_PORT_GPIO << 5) | (RIGHT_SENSOR_B_PIN_GPIO << 8) | ( RIGHT_SENSOR_B_PORT_GPIO << 13) | (LEFT_SENSOR_A_PIN_GPIO << 16)
	| ( LEFT_SENSOR_A_PORT_GPIO << 21) | (LEFT_SENSOR_B_PIN_GPIO << 24) | ( LEFT_SENSOR_B_PORT_GPIO << 29);

	LPC_GPIO_PIN_INT->ISEL = 0;
	LPC_GPIO_PIN_INT->IENR = 0xF;
	LPC_GPIO_PIN_INT->IENF = 0xF;

	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
	NVIC_EnableIRQ(PIN_INT3_IRQn);
#endif
}

void MiniRobInit() {
//Register init function
	sensorsTimers[SENSOR_ID].init = sensorInit;
	sensorsTimers[SENSOR_ID].refresh = reportValues;

	left.wheelStatus = 0;
	left.errorCount = 0;

	right.wheelStatus = 0;
	right.errorCount = 0;

	Chip_SCU_PinMuxSet(RIGHT_SENSOR_A_PORT, RIGHT_SENSOR_A_PIN, SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, RIGHT_SENSOR_A_PORT_GPIO, RIGHT_SENSOR_A_PIN_GPIO);

	Chip_SCU_PinMuxSet(RIGHT_SENSOR_B_PORT, RIGHT_SENSOR_B_PIN, SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, RIGHT_SENSOR_B_PORT_GPIO, RIGHT_SENSOR_B_PIN_GPIO);

	Chip_SCU_PinMuxSet(LEFT_SENSOR_A_PORT, LEFT_SENSOR_A_PIN, SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LEFT_SENSOR_A_PORT_GPIO, LEFT_SENSOR_A_PIN_GPIO);

	Chip_SCU_PinMuxSet(LEFT_SENSOR_B_PORT, LEFT_SENSOR_B_PIN, SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LEFT_SENSOR_B_PORT_GPIO, LEFT_SENSOR_B_PIN_GPIO);

	left.phaseA = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LEFT_SENSOR_A_PORT_GPIO, LEFT_SENSOR_A_PIN_GPIO);
	left.phaseB = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LEFT_SENSOR_B_PORT_GPIO, LEFT_SENSOR_B_PIN_GPIO);
	right.phaseA = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, RIGHT_SENSOR_A_PORT_GPIO, RIGHT_SENSOR_A_PIN_GPIO);
	right.phaseB = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, RIGHT_SENSOR_B_PORT_GPIO, RIGHT_SENSOR_B_PIN_GPIO);

	if (left.phaseA) {
		left.previousState = left.phaseB ? 2 : 3;
	} else {
		left.previousState = left.phaseB ? 1 : 0;
	}

	if (right.phaseA) {
		right.previousState = right.phaseB ? 2 : 3;
	} else {
		right.previousState = right.phaseB ? 1 : 0;
	}

}

#if 0
void GPIO0_IRQHandler(void) {
	uint32_t currentState;
	__disable_irq();
	if ( LPC_GPIO_PIN_INT->RISE & (1 << RIGHT_SENSOR_A_POSITION)) {
		LPC_GPIO_PIN_INT->RISE |= (1 << RIGHT_SENSOR_A_POSITION);
		right.phaseA = 1;
		if (right.phaseB) {
			currentState = 2;
		} else {
			currentState = 3;
		}
		if (currentState == ((right.previousState + 1) % 4)) {
			right.wheelStatus++;
		} else if (right.previousState == ((currentState + 1) % 4)) {
			right.wheelStatus--;
		} else {
			right.errorCount++;
		}
		right.previousState = currentState;
	} else if ( LPC_GPIO_PIN_INT->FALL & (1 << RIGHT_SENSOR_A_POSITION)) {
		LPC_GPIO_PIN_INT->FALL |= (1 << RIGHT_SENSOR_A_POSITION);
		right.phaseA = 0;
		if (right.phaseB) {
			currentState = 1;
		} else {
			currentState = 0;
		}
		if (currentState == ((right.previousState + 1) % 4)) {
			right.wheelStatus++;
		} else if (right.previousState == ((currentState + 1) % 4)) {
			right.wheelStatus--;
		} else {
			right.errorCount++;
		}
		right.previousState = currentState;
	}
	__enable_irq();
}
void GPIO1_IRQHandler(void) {
	uint32_t currentState;
	__disable_irq();
	if ( LPC_GPIO_PIN_INT->RISE & (1 << RIGHT_SENSOR_B_POSITION)) {
		LPC_GPIO_PIN_INT->RISE |= (1 << RIGHT_SENSOR_B_POSITION);
		right.phaseB = 1;
		if (right.phaseA) {
			currentState = 2;
		} else {
			currentState = 1;
		}
		if (currentState == ((right.previousState + 1) % 4)) {
			right.wheelStatus++;
		} else if (right.previousState == ((currentState + 1) % 4)) {
			right.wheelStatus--;
		} else {
			right.errorCount++;
		}
		right.previousState = currentState;

	} else if ( LPC_GPIO_PIN_INT->FALL & (1 << RIGHT_SENSOR_B_POSITION)) {
		LPC_GPIO_PIN_INT->FALL |= (1 << RIGHT_SENSOR_B_POSITION);
		right.phaseB = 0;
		if (right.phaseA) {
			currentState = 3;
		} else {
			currentState = 0;
		}
		if (currentState == ((right.previousState + 1) % 4)) {
			right.wheelStatus++;
		} else if (right.previousState == ((currentState + 1) % 4)) {
			right.wheelStatus--;
		} else {
			right.errorCount++;
		}
		right.previousState = currentState;
	}
	__enable_irq();
}
void GPIO2_IRQHandler(void) {
	uint32_t currentState;
	__disable_irq();
	if ( LPC_GPIO_PIN_INT->RISE & (1 << LEFT_SENSOR_A_POSITION)) {
		LPC_GPIO_PIN_INT->RISE |= (1 << LEFT_SENSOR_A_POSITION);
		left.phaseA = 1;
		if (left.phaseB) {
			currentState = 2;
		} else {
			currentState = 3;
		}
		if (currentState == ((left.previousState + 1) % 4)) {
			left.wheelStatus++;
		} else if (left.previousState == ((currentState + 1) % 4)) {
			left.wheelStatus--;
		} else {
			left.errorCount++;
		}
		left.previousState = currentState;
	} else if ( LPC_GPIO_PIN_INT->FALL & (1 << LEFT_SENSOR_A_POSITION)) {
		LPC_GPIO_PIN_INT->FALL |= (1 << LEFT_SENSOR_A_POSITION);
		left.phaseA = 0;
		if (left.phaseB) {
			currentState = 1;
		} else {
			currentState = 0;
		}
		if (currentState == ((left.previousState + 1) % 4)) {
			left.wheelStatus++;
		} else if (left.previousState == ((currentState + 1) % 4)) {
			left.wheelStatus--;
		} else {
			left.errorCount++;
		}
		left.previousState = currentState;
	}
	__enable_irq();
}
void GPIO3_IRQHandler(void) {
	uint32_t currentState;
	__disable_irq();
	if ( LPC_GPIO_PIN_INT->RISE & (1 << LEFT_SENSOR_B_POSITION)) {
		LPC_GPIO_PIN_INT->RISE |= (1 << LEFT_SENSOR_B_POSITION);
		left.phaseB = 1;
		if (left.phaseA) {
			currentState = 2;
		} else {
			currentState = 1;
		}
		if (currentState == ((left.previousState + 1) % 4)) {
			left.wheelStatus++;
		} else if (right.previousState == ((currentState + 1) % 4)) {
			left.wheelStatus--;
		} else {
			left.errorCount++;
		}
		right.previousState = currentState;
	} else if ( LPC_GPIO_PIN_INT->FALL & (1 << LEFT_SENSOR_B_POSITION)) {
		LPC_GPIO_PIN_INT->FALL |= (1 << LEFT_SENSOR_B_POSITION);
		left.phaseB = 0;
		if (left.phaseA) {
			currentState = 3;
		} else {
			currentState = 0;
		}
		if (currentState == ((left.previousState + 1) % 4)) {
			left.wheelStatus++;
		} else if (left.previousState == ((currentState + 1) % 4)) {
			left.wheelStatus--;
		} else {
			left.errorCount++;
		}
		left.previousState = currentState;
	}
	__enable_irq();
}

#endif

