#include "EDVS128_LPC43xx.h"
#include "chip.h"
#include "extra_pins.h"
#include "sleep.h"
#include <cr_section_macros.h>

#define DEFAULT_BIAS_SET		BIAS_DEFAULT
// *****************************************************************************
uint32_t biasMatrix[12];
// *****************************************************************************
uint32_t eDVSDataFormat = EDVS_DATA_FORMAT_DEFAULT;
uint32_t enableEventSending;

uint32_t shouldRecord = 0;
volatile uint32_t lastEventCount = 0;

__DATA(RAM2) volatile uint16_t eventBufferA[DVS_EVENTBUFFER_SIZE];	// for event addresses
__DATA(RAM3) volatile uint32_t eventBufferT[DVS_EVENTBUFFER_SIZE];	// for time stamps
__DATA(RAM7) volatile uint32_t eventBufferWritePointer = 0;
__DATA(RAM8) volatile uint32_t eventBufferReadPointer = 0;
__DATA(RAM9) volatile uint32_t currentEventRate = 0;

// *****************************************************************************
void DVS128ChipInit(void) {

	// *****************************************************************************
	// ** initialize Timer 1 (system main clock)
	// *****************************************************************************
	//TODO: instead of changing the prescaler, reduce peripheral clock to save energy
	Chip_TIMER_Init(LPC_TIMER1);
	Chip_TIMER_PrescaleSet(LPC_TIMER1, 2);	// prescaler: run at 64Mhz!
	Chip_TIMER_MatchDisableInt(LPC_TIMER1, TIMER_CAPTURE_CHANNEL);
	Chip_TIMER_ResetOnMatchDisable(LPC_TIMER1, TIMER_CAPTURE_CHANNEL);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, TIMER_CAPTURE_CHANNEL);

	Chip_TIMER_CaptureRisingEdgeDisable(LPC_TIMER1, TIMER_CAPTURE_CHANNEL);
	Chip_TIMER_CaptureFallingEdgeEnable(LPC_TIMER1, TIMER_CAPTURE_CHANNEL);
	Chip_TIMER_CaptureDisableInt(LPC_TIMER1, TIMER_CAPTURE_CHANNEL);

	// set P5.0 to capture register CAP1_0
	Chip_SCU_PinMuxSet(PORT_DVS_REQUEST, PIN_DVS_REQUEST, MD_BUK | MD_EZI | FUNC5);
	LPC_GIMA->CAP0_IN[1][1] = (uint32_t) (0x2 << 4);

	Chip_TIMER_Enable(LPC_TIMER1);  //Enable timer1

	// *****************************************************************************
	enableEventSending = 0;
	eDVSDataFormat = EDVS_DATA_FORMAT_DEFAULT;

	Chip_SCU_PinMuxSet(PORT_RX_DEFAULT_GND, PIN_RX_DEFAULT_GND, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_RX_DEFAULT_GND, GPIO_PIN_RX_DEFAULT_GND);	// set to ground
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RX_DEFAULT_GND, GPIO_PIN_RX_DEFAULT_GND); /* set P4.0 as output */

	Chip_SCU_PinMuxSet(PORT_RESET_DVS, PIN_RESET_DVS, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RESET_DVS, GPIO_PIN_RESET_DVS); /* set P2.11 as output */
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_RESET_DVS, GPIO_PIN_RESET_DVS); // DVS array reset to high

	Chip_SCU_PinMuxSet(PORT_DVS_OE, PIN_DVS_OE, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_DVS_OE, GPIO_PIN_DVS_OE); /* set P2.9 as output */
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_DVS_OE, GPIO_PIN_DVS_OE); // Enable DVS outputs

	// let DVS handshake itself (REQ -> ACK)
	// set ACK as input
	Chip_SCU_PinMuxSet(PORT_DVS_ACKN, PIN_DVS_ACKN, MD_BUK | MD_EZI | FUNC0);

	Chip_SCU_PinMuxSet(PORT_BIAS_LATCH, PIN_BIAS_LATCH, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_BIAS_LATCH, GPIO_PIN_BIAS_LATCH);	// set pins to bias setup as outputs
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_BIAS_LATCH, GPIO_PIN_BIAS_LATCH); /* set P2.12 as output */

	Chip_SCU_PinMuxSet(PORT_BIAS_DATA, PIN_BIAS_DATA, MD_PLN_FAST | FUNC4);
	Chip_SCU_PinMuxSet(PORT_BIAS_CLOCK, PIN_BIAS_CLOCK, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_BIAS_DATA, GPIO_PIN_BIAS_DATA);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_BIAS_CLOCK, GPIO_PIN_BIAS_CLOCK);
	/* set P3.4 and P3.7 as output */
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_BIAS_DATA, GPIO_PIN_BIAS_DATA);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_BIAS_CLOCK, GPIO_PIN_BIAS_CLOCK);

	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_RESET_DVS, GPIO_PIN_RESET_DVS); // DVS array reset to low
	timer_delay_ms(10); 	 								// 10ms delay
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_RESET_DVS, GPIO_PIN_RESET_DVS); // DVS array reset to high
	timer_delay_ms(1); 	 								// 1ms delay

	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X0, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X1, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X2, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X3, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X4, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X5, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_X6, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_X, PIN_EVENT_P, MD_BUK | MD_EZI | FUNC0);

	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y0, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y1, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y2, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y3, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y4, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y5, MD_BUK | MD_EZI | FUNC0);
	Chip_SCU_PinMuxSet(PORT_EVENT_Y, PIN_EVENT_Y6, MD_BUK | MD_EZI | FUNC0);

	DVS128BiasLoadDefaultSet(DEFAULT_BIAS_SET);	// load default bias settings
	DVS128BiasFlush(1);					// transfer bias settings to chip

}

// *****************************************************************************
void DVS128FetchEventsEnable(unsigned char flag) {
	if (flag) {
		enableEventSending = 1;
	} else {
		enableEventSending = 0;
	}
}

// *****************************************************************************
void DVS128BiasSet(uint32_t biasID, uint32_t biasValue) {
	if (biasID < 12) {
		biasMatrix[biasID] = biasValue;
	}
}
// *****************************************************************************
uint32_t DVS128BiasGet(uint32_t biasID) {
	if (biasID < 12) {
		return (biasMatrix[biasID]);
	}
	return (0);
}

// *****************************************************************************
void DVS128BiasLoadDefaultSet(uint32_t biasSetID) {

	switch (biasSetID) {

	case 0: // 12 bias values of 24 bits each 								BIAS_DEFAULT
		biasMatrix[0] = 1067; // 0x00042B,	  		// Tmpdiff128.IPot.cas
		biasMatrix[1] = 12316; // 0x00301C,			// Tmpdiff128.IPot.injGnd
		biasMatrix[2] = 16777215; // 0xFFFFFF,			// Tmpdiff128.IPot.reqPd
		biasMatrix[3] = 5579732; // 0x5523D4,			// Tmpdiff128.IPot.puX
		biasMatrix[4] = 151; // 0x000097,			// Tmpdiff128.IPot.diffOff
		biasMatrix[5] = 427594; // 0x06864A,			// Tmpdiff128.IPot.req
		biasMatrix[6] = 0; // 0x000000,			// Tmpdiff128.IPot.refr
		biasMatrix[7] = 16777215; // 0xFFFFFF,			// Tmpdiff128.IPot.puY
		biasMatrix[8] = 296253; // 0x04853D,			// Tmpdiff128.IPot.diffOn
		biasMatrix[9] = 3624; // 0x000E28,			// Tmpdiff128.IPot.diff
		biasMatrix[10] = 39; // 0x000027,			// Tmpdiff128.IPot.foll
		biasMatrix[11] = 4; // 0x000004			// Tmpdiff128.IPot.Pr
		break;

	case 1: // 12 bias values of 24 bits each 								BIAS_BRAGFOST
		biasMatrix[0] = 1067;	  		// Tmpdiff128.IPot.cas
		biasMatrix[1] = 12316;			// Tmpdiff128.IPot.injGnd
		biasMatrix[2] = 16777215;			// Tmpdiff128.IPot.reqPd
		biasMatrix[3] = 5579731;			// Tmpdiff128.IPot.puX
		biasMatrix[4] = 60;			// Tmpdiff128.IPot.diffOff
		biasMatrix[5] = 427594;			// Tmpdiff128.IPot.req
		biasMatrix[6] = 0;			// Tmpdiff128.IPot.refr
		biasMatrix[7] = 16777215;			// Tmpdiff128.IPot.puY
		biasMatrix[8] = 567391;			// Tmpdiff128.IPot.diffOn
		biasMatrix[9] = 6831;			// Tmpdiff128.IPot.diff
		biasMatrix[10] = 39;			// Tmpdiff128.IPot.foll
		biasMatrix[11] = 4;			// Tmpdiff128.IPot.Pr
		break;

	case 2: // 12 bias values of 24 bits each 								BIAS_FAST
		biasMatrix[0] = 1966;	  		// Tmpdiff128.IPot.cas
		biasMatrix[1] = 1137667;			// Tmpdiff128.IPot.injGnd
		biasMatrix[2] = 16777215;			// Tmpdiff128.IPot.reqPd
		biasMatrix[3] = 8053457;			// Tmpdiff128.IPot.puX
		biasMatrix[4] = 133;			// Tmpdiff128.IPot.diffOff
		biasMatrix[5] = 160712;			// Tmpdiff128.IPot.req
		biasMatrix[6] = 944;			// Tmpdiff128.IPot.refr
		biasMatrix[7] = 16777215;			// Tmpdiff128.IPot.puY
		biasMatrix[8] = 205255;			// Tmpdiff128.IPot.diffOn
		biasMatrix[9] = 3207;			// Tmpdiff128.IPot.diff
		biasMatrix[10] = 278;			// Tmpdiff128.IPot.foll
		biasMatrix[11] = 217;			// Tmpdiff128.IPot.Pr
		break;

	case 3: // 12 bias values of 24 bits each 								BIAS_STEREO_PAIR
		biasMatrix[0] = 1966;
		biasMatrix[1] = 1135792;
		biasMatrix[2] = 16769632;
		biasMatrix[3] = 8061894;
		biasMatrix[4] = 133;
		biasMatrix[5] = 160703;
		biasMatrix[6] = 935;
		biasMatrix[7] = 16769632;
		biasMatrix[8] = 205244;
		biasMatrix[9] = 3207;
		biasMatrix[10] = 267;
		biasMatrix[11] = 217;
		break;

	case 4: // 12 bias values of 24 bits each 								BIAS_MINI_DVS
		biasMatrix[0] = 1966;
		biasMatrix[1] = 1137667;
		biasMatrix[2] = 16777215;
		biasMatrix[3] = 8053458;
		biasMatrix[4] = 62;
		biasMatrix[5] = 160712;
		biasMatrix[6] = 944;
		biasMatrix[7] = 16777215;
		biasMatrix[8] = 480988;
		biasMatrix[9] = 3207;
		biasMatrix[10] = 278;
		biasMatrix[11] = 217;
		break;

	case 5: // 12 bias values of 24 bits each 								BIAS_BRAGFOST - on/off balanced
		biasMatrix[0] = 1067;	  		// Tmpdiff128.IPot.cas
		biasMatrix[1] = 12316;			// Tmpdiff128.IPot.injGnd
		biasMatrix[2] = 16777215;			// Tmpdiff128.IPot.reqPd
		biasMatrix[3] = 5579731;			// Tmpdiff128.IPot.puX
		biasMatrix[4] = 60;			// Tmpdiff128.IPot.diffOff
		biasMatrix[5] = 427594;			// Tmpdiff128.IPot.req
		biasMatrix[6] = 0;			// Tmpdiff128.IPot.refr
		biasMatrix[7] = 16777215;			// Tmpdiff128.IPot.puY
		biasMatrix[8] = 567391;			// Tmpdiff128.IPot.diffOn
		biasMatrix[9] = 19187;			// Tmpdiff128.IPot.diff
		biasMatrix[10] = 39;			// Tmpdiff128.IPot.foll
		biasMatrix[11] = 4;			// Tmpdiff128.IPot.Pr
		break;

	}
}

// *****************************************************************************
#define BOUT(x)  { if (x) \
						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO_PORT_BIAS_DATA, GPIO_PIN_BIAS_DATA);\
					else \
						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO_PORT_BIAS_DATA, GPIO_PIN_BIAS_DATA); \
						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO_PORT_BIAS_CLOCK, GPIO_PIN_BIAS_CLOCK);\
						timer_delay_us(10);\
						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO_PORT_BIAS_CLOCK, GPIO_PIN_BIAS_CLOCK);\
						timer_delay_us(10); }

void DVS128BiasFlush(uint32_t multiplier) {
	uint32_t biasIndex, currentBias;

	for (biasIndex = 0; biasIndex < 12; biasIndex++) {
		currentBias = biasMatrix[biasIndex];

		currentBias *= multiplier;
		if (currentBias > 0xFFFFFF)
			currentBias = 0xFFFFFF;

		BOUT(currentBias & 0x800000);
		BOUT(currentBias & 0x400000);
		BOUT(currentBias & 0x200000);
		BOUT(currentBias & 0x100000);

		BOUT(currentBias & 0x80000);
		BOUT(currentBias & 0x40000);
		BOUT(currentBias & 0x20000);
		BOUT(currentBias & 0x10000);

		BOUT(currentBias & 0x8000);
		BOUT(currentBias & 0x4000);
		BOUT(currentBias & 0x2000);
		BOUT(currentBias & 0x1000);

		BOUT(currentBias & 0x800);
		BOUT(currentBias & 0x400);
		BOUT(currentBias & 0x200);
		BOUT(currentBias & 0x100);

		BOUT(currentBias & 0x80);
		BOUT(currentBias & 0x40);
		BOUT(currentBias & 0x20);
		BOUT(currentBias & 0x10);

		BOUT(currentBias & 0x8);
		BOUT(currentBias & 0x4);
		BOUT(currentBias & 0x2);
		BOUT(currentBias & 0x1);

	}  // end of biasIndexclocking

	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_BIAS_DATA, GPIO_PIN_BIAS_DATA); // set data pin to low just to have the same output all the time

// trigger latch to push bias data to bias generators
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_BIAS_LATCH, GPIO_PIN_BIAS_LATCH);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_BIAS_LATCH, GPIO_PIN_BIAS_LATCH);

}

void RTC_IRQHandler(void) {
	Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	lastEventCount = currentEventRate;
	__DSB(); //Doesn't work without this.
	currentEventRate = 0;
}

void RTCInit() {
	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_CntIncrIntConfig(LPC_RTC, _BIT(RTC_TIMETYPE_SECOND), ENABLE);
	Chip_RTC_Enable(LPC_RTC, ENABLE);
	NVIC_EnableIRQ(RTC_IRQn);
}

void getFilename(char * filename, uint32_t DVSEventTime) {
	uint16_t tmp;
	filename[0] = '/';
	tmp = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_YEAR) & RTC_YEAR_MASK;
	filename[1] = '0' + tmp / 1000;
	tmp %= 1000;
	filename[2] = '0' + tmp / 100;
	tmp %= 100;
	filename[3] = '0' + tmp / 10;
	tmp %= 10;
	filename[4] = '0' + tmp;
	tmp = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_MONTH) & RTC_MONTH_MASK;
	filename[5] = '0' + tmp / 10;
	tmp %= 10;
	filename[6] = '0' + tmp;
	tmp = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_DAYOFMONTH) & RTC_DOM_MASK;
	filename[7] = '0' + tmp / 10;
	tmp %= 10;
	filename[8] = '0' + tmp;
	tmp = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_HOUR) & RTC_HOUR_MASK;
	filename[9] = '0' + tmp / 10;
	tmp %= 10;
	filename[10] = '0' + tmp;
	tmp = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_MINUTE) & RTC_MIN_MASK;
	filename[11] = '0' + tmp / 10;
	tmp %= 10;
	filename[12] = '0' + tmp;
	tmp = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_SECOND) & RTC_SEC_MASK;
	filename[13] = '0' + tmp / 10;
	tmp %= 10;
	filename[14] = '0' + tmp;
	tmp = DVSEventTime % 1000;
	filename[15] = '0' + tmp / 100;
	tmp = DVSEventTime % 100;
	filename[16] = '0' + tmp / 10;
	tmp = DVSEventTime % 10;
	filename[17] = '0' + tmp;
	filename[18] = '\0';
}

