/*
 * minirob.h
 *
 *  Created on: 13/04/2014
 *      Author: ruka
 */

#ifndef MINIROB_H_
#define MINIROB_H_

extern void MiniRobInit();
extern void refreshMiniRobSensors();
#endif /* MINIROB_H_ */
