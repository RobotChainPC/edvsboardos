/*
 * motors.h
 *
 *  Created on: 4 de Dez de 2012
 *      Author: ruka
 */

#ifndef MOTORS_H_
#define MOTORS_H_

#include <stdint.h>

extern void initMotors(void);
//Use to sample the encoders
extern void updatePWMPeriod(uint32_t motor, uint32_t period);
extern void updateMotorVelocity(uint32_t motor, int32_t speed);
extern void updateMotorDutyCycle(uint32_t direction, int32_t duty_cycle);
extern void enableMotorDriver(uint8_t enable);
struct motor_control {
	int16_t previousAdc;
	int32_t current_dutycycle;
	int32_t requested_dutycycle;
	int32_t current_pwm_input;
	int32_t totalError;
	int32_t previousError;
	uint8_t direction; // 1 - forward, 0 - backward
};
extern int32_t motors_sensors[];
extern volatile struct motor_control motor0;
extern volatile struct motor_control motor1;

#endif /* MOTORS_H_ */
