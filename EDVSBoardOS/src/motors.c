#include "motors.h"
#include "chip.h"
//192Mhz / 7680 = 25kHz
#define BASE_PWM_DIVIDER				(7680)

#define MOTOR_DRIVER_ENABLE_PORT 		(6)
#define MOTOR_DRIVER_ENABLE_PIN			(6)
#define MOTOR_DRIVER_ENABLE_PORT_GPIO	(0)
#define MOTOR_DRIVER_ENABLE_PIN_GPIO	(5)

#define MOTOR_DRIVER_FAULT_PORT 		(1)
#define MOTOR_DRIVER_FAULT_PIN			(18)
#define MOTOR_DRIVER_FAULT_PORT_GPIO	(0)
#define MOTOR_DRIVER_FAULT_PIN_GPIO		(13)

#define MOTOR0						(0)
#define MOTOR0_PWM_CHANNEL 			(2)
#define MOTOR0_PWM_1_PORT 			(5)
#define MOTOR0_PWM_1_PIN 			(7)
#define MOTOR0_PWM_1_PORT_GPIO		(2)
#define MOTOR0_PWM_1_PIN_GPIO		(7)
#define MOTOR0_PWM_2_PORT 			(5)
#define MOTOR0_PWM_2_PIN			(0)
#define MOTOR0_PWM_2_PORT_GPIO		(2)
#define MOTOR0_PWM_2_PIN_GPIO		(9)

#define MOTOR1						(1)
#define MOTOR1_PWM_CHANNEL 			(1)
#define MOTOR1_PWM_1_PORT 			(5)
#define MOTOR1_PWM_1_PIN 			(5)
#define MOTOR1_PWM_1_PORT_GPIO		(2)
#define MOTOR1_PWM_1_PIN_GPIO		(14)
#define MOTOR1_PWM_2_PORT 			(5)
#define MOTOR1_PWM_2_PIN			(6)
#define MOTOR1_PWM_2_PORT_GPIO		(2)
#define MOTOR1_PWM_2_PIN_GPIO		(15)

/** Edge aligned mode for channel in MCPWM */
#define MCPWM_CHANNEL_EDGE_MODE			((uint32_t)(0))
/** Center aligned mode for channel in MCPWM */
#define MCPWM_CHANNEL_CENTER_MODE		((uint32_t)(1))

/** Polarity of the MCOA and MCOB pins: Passive state is LOW, active state is HIGH */
#define MCPWM_CHANNEL_PASSIVE_LO		((uint32_t)(0))
/** Polarity of the MCOA and MCOB pins: Passive state is HIGH, active state is LOW */
#define MCPWM_CHANNEL_PASSIVE_HI		((uint32_t)(1))

/* Output Patent in 3-phase DC mode, the internal MCOA0 signal is routed to any or all of
 * the six output pins under the control of the bits in this register */
#define MCPWM_PATENT_A0		((uint32_t)(1<<0))	/**< MCOA0 tracks internal MCOA0 */
#define MCPWM_PATENT_B0		((uint32_t)(1<<1))	/**< MCOB0 tracks internal MCOA0 */
#define MCPWM_PATENT_A1		((uint32_t)(1<<2))	/**< MCOA1 tracks internal MCOA0 */
#define MCPWM_PATENT_B1		((uint32_t)(1<<3))	/**< MCOB1 tracks internal MCOA0 */
#define MCPWM_PATENT_A2		((uint32_t)(1<<4))	/**< MCOA2 tracks internal MCOA0 */
#define MCPWM_PATENT_B2		((uint32_t)(1<<5))	/**< MCOB2 tracks internal MCOA0 */

/*********************************************************************//**
 * Macro defines for MCPWM Interrupt register
 **********************************************************************/
/* Interrupt registers, these macro definitions below can be applied for these
 * register type:
 * - MCPWM Interrupt Enable read address
 * - MCPWM Interrupt Enable set address
 * - MCPWM Interrupt Enable clear address
 * - MCPWM Interrupt Flags read address
 * - MCPWM Interrupt Flags set address
 * - MCPWM Interrupt Flags clear address
 */
/** Limit interrupt for channel (n) */
#define MCPWM_INT_ILIM(n)	(((n>=0)&&(n<=2)) ? ((uint32_t)(1<<((n*4)+0))) : (0))
/** Match interrupt for channel (n) */
#define MCPWM_INT_IMAT(n)	(((n>=0)&&(n<=2)) ? ((uint32_t)(1<<((n*4)+1))) : (0))
/** Capture interrupt for channel (n) */
#define MCPWM_INT_ICAP(n)	(((n>=0)&&(n<=2)) ? ((uint32_t)(1<<((n*4)+2))) : (0))
/** Fast abort interrupt */
#define MCPWM_INT_ABORT		((uint32_t)(1<<15))

/*********************************************************************//**
 * Macro defines for MCPWM Capture clear address register
 **********************************************************************/
/** Clear the MCCAP (n) register */
#define MCPWM_CAPCLR_CAP(n)		(((n<=2)) ? ((uint32_t)(1<<n)) : (0))

/* Interrupt type in MCPWM */
/** Limit interrupt for channel (0) */
#define MCPWM_INTFLAG_LIM0	MCPWM_INT_ILIM(0)
/** Match interrupt for channel (0) */
#define MCPWM_INTFLAG_MAT0	MCPWM_INT_IMAT(0)
/** Capture interrupt for channel (0) */
#define MCPWM_INTFLAG_CAP0	MCPWM_INT_ICAP(0)

/** Limit interrupt for channel (1) */
#define MCPWM_INTFLAG_LIM1	MCPWM_INT_ILIM(1)
/** Match interrupt for channel (1) */
#define MCPWM_INTFLAG_MAT1	MCPWM_INT_IMAT(1)
/** Capture interrupt for channel (1) */
#define MCPWM_INTFLAG_CAP1	MCPWM_INT_ICAP(1)

/** Limit interrupt for channel (2) */
#define MCPWM_INTFLAG_LIM2	MCPWM_INT_ILIM(2)
/** Match interrupt for channel (2) */
#define MCPWM_INTFLAG_MAT2	MCPWM_INT_IMAT(2)
/** Capture interrupt for channel (2) */
#define MCPWM_INTFLAG_CAP2	MCPWM_INT_ICAP(2)

/** Fast abort interrupt */
#define MCPWM_INTFLAG_ABORT	MCPWM_INT_ABORT

/*********************************************************************//**
 * Macro defines for MCPWM Control register
 **********************************************************************/
/* MCPWM Control register, these macro definitions below can be applied for these
 * register type:
 * - MCPWM Control read address
 * - MCPWM Control set address
 * - MCPWM Control clear address
 */
/**< Stops/starts timer channel n */
#define MCPWM_CON_RUN(n)		((uint32_t)(1<<((n*8)+0)))
/**< Edge/center aligned operation for channel n */
#define MCPWM_CON_CENTER(n)		((uint32_t)(1<<((n*8)+1)))
/**< Select polarity of the MCOAn and MCOBn pin */
#define MCPWM_CON_POLAR(n)		((uint32_t)(1<<((n*8)+2)))
/**< Control the dead-time feature for channel n */
#define MCPWM_CON_DTE(n)		((uint32_t)(1<<((n*8)+3)))
/**< Enable/Disable update of functional register for channel n */
#define MCPWM_CON_DISUP(n)		((uint32_t)(1<<((n*8)+4)))
/**< Control the polarity for all 3 channels */
#define MCPWM_CON_INVBDC		((uint32_t)(1<<29))
/**< 3-phase AC mode select */
#define MCPWM_CON_ACMODE		((uint32_t)(1<<30))
/**< 3-phase DC mode select */
#define MCPWM_CON_DCMODE		(((uint32_t)1<<31))

volatile struct motor_control motor0;
volatile struct motor_control motor1;

int32_t motors_sensors[2];

static uint32_t motorDriverEnabled;

void updatePWMPeriod(uint32_t motor, uint32_t period) {
	if (motor == MOTOR0) {
		LPC_MCPWM->LIM[MOTOR0_PWM_CHANNEL] = Chip_Clock_GetRate(CLK_APB1_MOTOCON) / period;
		return;
	}
	if (motor == MOTOR1) {
		LPC_MCPWM->LIM[MOTOR1_PWM_CHANNEL] = Chip_Clock_GetRate(CLK_APB1_MOTOCON) / period;
		return;
	}

}

void updateMotorVelocity(uint32_t motor, int32_t speed) {
	if (motor == MOTOR0) {
		motor0.requested_dutycycle = speed;
	} else if (motor == MOTOR1) {
		motor1.requested_dutycycle = speed;
	}
	updateMotorDutyCycle(motor, speed);
}

void updateMotorDutyCycle(uint32_t motor, int32_t duty_cycle) {
	if (motor == MOTOR0) {
		motor0.current_dutycycle = duty_cycle;
		if (duty_cycle == 0) {
			//Brake
			Chip_SCU_PinMuxSet(MOTOR0_PWM_1_PORT, MOTOR0_PWM_1_PIN, MD_PLN_FAST | FUNC0);
			Chip_SCU_PinMuxSet(MOTOR0_PWM_2_PORT, MOTOR0_PWM_2_PIN, MD_PLN_FAST | FUNC0);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR0_PWM_1_PORT_GPIO, MOTOR0_PWM_1_PIN_GPIO);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR0_PWM_2_PORT_GPIO, MOTOR0_PWM_2_PIN_GPIO);
		}
		//Moving forward
		else if (duty_cycle > 0) {
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR0_PWM_2_PORT_GPIO, MOTOR0_PWM_2_PIN_GPIO);
			LPC_MCPWM->MAT[MOTOR0_PWM_CHANNEL] = ((duty_cycle * LPC_MCPWM->LIM[MOTOR0_PWM_CHANNEL]) / 100);
			Chip_SCU_PinMuxSet(MOTOR0_PWM_1_PORT, MOTOR0_PWM_1_PIN, MD_PLN_FAST | FUNC1);
			Chip_SCU_PinMuxSet(MOTOR0_PWM_2_PORT, MOTOR0_PWM_2_PIN, MD_PLN_FAST | FUNC0);
		} //Moving backwards
		else {
			duty_cycle = -duty_cycle;
			//Speed is negative
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR0_PWM_1_PORT_GPIO, MOTOR0_PWM_1_PIN_GPIO);
			LPC_MCPWM->MAT[MOTOR0_PWM_CHANNEL] = LPC_MCPWM->LIM[MOTOR0_PWM_CHANNEL] - ((duty_cycle * LPC_MCPWM->LIM[MOTOR1_PWM_CHANNEL]) / 100);
			Chip_SCU_PinMuxSet(MOTOR0_PWM_1_PORT, MOTOR0_PWM_1_PIN, MD_PLN_FAST | FUNC0);
			Chip_SCU_PinMuxSet(MOTOR0_PWM_2_PORT, MOTOR0_PWM_2_PIN, MD_PLN_FAST | FUNC1);
		}
		return;
	}
	if (motor == MOTOR1) {
		motor1.current_dutycycle = duty_cycle;
		if (duty_cycle == 0) {
			//Brake
			Chip_SCU_PinMuxSet(MOTOR1_PWM_1_PORT, MOTOR1_PWM_1_PIN, MD_PLN_FAST | FUNC0);
			Chip_SCU_PinMuxSet(MOTOR1_PWM_2_PORT, MOTOR1_PWM_2_PIN, MD_PLN_FAST | FUNC0);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR1_PWM_1_PORT_GPIO, MOTOR1_PWM_1_PIN_GPIO);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR1_PWM_2_PORT_GPIO, MOTOR1_PWM_2_PIN_GPIO);
		}
		//Moving forward
		else if (duty_cycle > 0) {
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR1_PWM_2_PORT_GPIO, MOTOR1_PWM_2_PIN_GPIO);
			LPC_MCPWM->MAT[MOTOR1_PWM_CHANNEL] = ((duty_cycle * LPC_MCPWM->LIM[MOTOR1_PWM_CHANNEL]) / 100);
			Chip_SCU_PinMuxSet(MOTOR1_PWM_1_PORT, MOTOR1_PWM_1_PIN, MD_PLN_FAST | FUNC1);
			Chip_SCU_PinMuxSet(MOTOR1_PWM_2_PORT, MOTOR1_PWM_2_PIN, MD_PLN_FAST | FUNC0);
		} //Moving backwards
		else {
			duty_cycle = -duty_cycle;
			//Speed is negative
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR1_PWM_1_PORT_GPIO, MOTOR1_PWM_1_PIN_GPIO);
			LPC_MCPWM->MAT[MOTOR1_PWM_CHANNEL] = LPC_MCPWM->LIM[MOTOR1_PWM_CHANNEL] - ((duty_cycle * LPC_MCPWM->LIM[MOTOR1_PWM_CHANNEL]) / 100);
			Chip_SCU_PinMuxSet(MOTOR1_PWM_1_PORT, MOTOR1_PWM_1_PIN, MD_PLN_FAST | FUNC0);
			Chip_SCU_PinMuxSet(MOTOR1_PWM_2_PORT, MOTOR1_PWM_2_PIN, MD_PLN_FAST | FUNC1);
		}
		return;
	}

}

void enableMotorDriver(uint8_t enable) {
	motorDriverEnabled = enable;
	if (motorDriverEnabled) {
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR_DRIVER_ENABLE_PORT_GPIO, MOTOR_DRIVER_ENABLE_PIN_GPIO);
		updateMotorDutyCycle(MOTOR0, motor0.current_dutycycle);
		updateMotorDutyCycle(MOTOR1, motor1.current_dutycycle);
	} else {
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, MOTOR_DRIVER_ENABLE_PORT_GPIO, MOTOR_DRIVER_ENABLE_PIN_GPIO);

	}
}

void initMotors(void) {
	Chip_Clock_Enable(CLK_APB1_MOTOCON);

	LPC_MCPWM->CAP_CLR = MCPWM_CAPCLR_CAP(0) | MCPWM_CAPCLR_CAP(1) | MCPWM_CAPCLR_CAP(2);

	LPC_MCPWM->INTF_CLR = MCPWM_INT_ILIM(0) | MCPWM_INT_ILIM(1) | MCPWM_INT_ILIM(2) | MCPWM_INT_IMAT(0) | MCPWM_INT_IMAT(1) | MCPWM_INT_IMAT(2) | MCPWM_INT_ICAP(0) | MCPWM_INT_ICAP(1)
			| MCPWM_INT_ICAP(2);

	LPC_MCPWM->INTEN_CLR = MCPWM_INT_ILIM(0) | MCPWM_INT_ILIM(1) | MCPWM_INT_ILIM(2) | MCPWM_INT_IMAT(0) | MCPWM_INT_IMAT(1) | MCPWM_INT_IMAT(2) | MCPWM_INT_ICAP(0) | MCPWM_INT_ICAP(1)
			| MCPWM_INT_ICAP(2);
	LPC_MCPWM->CON_CLR = MCPWM_CON_CENTER(MOTOR0_PWM_CHANNEL) | MCPWM_CON_CENTER(MOTOR1_PWM_CHANNEL);
	LPC_MCPWM->CON_CLR = MCPWM_CON_POLAR(MOTOR0_PWM_CHANNEL) | MCPWM_CON_POLAR(MOTOR1_PWM_CHANNEL);
	LPC_MCPWM->CON_CLR = MCPWM_CON_DTE(MOTOR0_PWM_CHANNEL) | MCPWM_CON_DTE(MOTOR1_PWM_CHANNEL);
	LPC_MCPWM->CON_CLR = MCPWM_CON_DISUP(MOTOR0_PWM_CHANNEL) | MCPWM_CON_DISUP(MOTOR1_PWM_CHANNEL);

	LPC_MCPWM->TC[MOTOR0_PWM_CHANNEL] = 0;
	LPC_MCPWM->TC[MOTOR1_PWM_CHANNEL] = 0;
	LPC_MCPWM->LIM[MOTOR0_PWM_CHANNEL] = BASE_PWM_DIVIDER; //192MHz/7680= 25KHz
	LPC_MCPWM->LIM[MOTOR1_PWM_CHANNEL] = BASE_PWM_DIVIDER; //192MHz/7680= 25KHz
	LPC_MCPWM->MAT[MOTOR0_PWM_CHANNEL] = BASE_PWM_DIVIDER;
	LPC_MCPWM->MAT[MOTOR1_PWM_CHANNEL] = BASE_PWM_DIVIDER;

	Chip_SCU_PinMuxSet(MOTOR0_PWM_1_PORT, MOTOR0_PWM_1_PIN, MD_PLN_FAST | FUNC0);
	Chip_SCU_PinMuxSet(MOTOR1_PWM_1_PORT, MOTOR1_PWM_1_PIN, MD_PLN_FAST | FUNC0);
	Chip_SCU_PinMuxSet(MOTOR0_PWM_2_PORT, MOTOR0_PWM_2_PIN, MD_PLN_FAST | FUNC0);
	Chip_SCU_PinMuxSet(MOTOR1_PWM_2_PORT, MOTOR1_PWM_2_PIN, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR0_PWM_1_PORT_GPIO, MOTOR0_PWM_1_PIN_GPIO);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR1_PWM_1_PORT_GPIO, MOTOR1_PWM_1_PIN_GPIO);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR0_PWM_2_PORT_GPIO, MOTOR0_PWM_2_PIN_GPIO);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR1_PWM_2_PORT_GPIO, MOTOR1_PWM_2_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR0_PWM_1_PORT_GPIO, MOTOR0_PWM_1_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR1_PWM_1_PORT_GPIO, MOTOR1_PWM_1_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR0_PWM_2_PORT_GPIO, MOTOR0_PWM_2_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR1_PWM_2_PORT_GPIO, MOTOR1_PWM_2_PIN_GPIO);

	motorDriverEnabled = 1;
	Chip_SCU_PinMuxSet(MOTOR_DRIVER_ENABLE_PORT, MOTOR_DRIVER_ENABLE_PIN, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR_DRIVER_ENABLE_PORT_GPIO, MOTOR_DRIVER_ENABLE_PIN_GPIO);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR_DRIVER_ENABLE_PORT_GPIO, MOTOR_DRIVER_ENABLE_PIN_GPIO);

	Chip_SCU_PinMuxSet(MOTOR_DRIVER_FAULT_PORT, MOTOR_DRIVER_FAULT_PIN, MD_BUK | MD_EZI | FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, MOTOR_DRIVER_FAULT_PORT_GPIO, MOTOR_DRIVER_FAULT_PIN_GPIO);

	LPC_MCPWM->CON_SET = MCPWM_CON_RUN(MOTOR1_PWM_CHANNEL) | MCPWM_CON_RUN(MOTOR0_PWM_CHANNEL);
//Initialize the control structure
	motor0.current_pwm_input = 0;
	motor1.current_pwm_input = 0;
	motor0.current_dutycycle = 0;
	motor1.current_dutycycle = 0;
	motor0.direction = 1;
	motor1.direction = 1;
	motor0.requested_dutycycle = 0;
	motor1.requested_dutycycle = 0;
}
