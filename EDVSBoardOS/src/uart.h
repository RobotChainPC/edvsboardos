#ifndef __UART_H 
#define __UART_H
#include "chip.h"

#define RTS0_PORT                		(1)
#define RTS0_PIN                		(3)
#define RTS0_GPIO_PORT                	(0)
#define RTS0_GPIO_PIN               	(10)

#define CTS0_PORT						(1)
#define CTS0_PIN						(5)
#define CTS0_GPIO_PORT					(1)
#define CTS0_GPIO_PIN					(8)

#define UART_PORT_DEFAULT (0)
#define BAUD_RATE_DEFAULT		((uint32_t) (4000000))

#define TX_BUFFER_SIZE_BITS		(10)
#define TX_BUFFER_SIZE			(1<<TX_BUFFER_SIZE_BITS)
#define TX_BUFFER_MASK			(TX_BUFFER_SIZE-1)

struct transmist_buffer {
	uint8_t buffer[TX_BUFFER_SIZE];
	uint32_t bufferWritePointer;
	uint32_t bufferReadPointer;
};
//Transmit buffer that will be used on the rest of the system
extern struct transmist_buffer tx_buffer;

#define PUSH_BYTE(tx_buffer, byte) {\
		(tx_buffer).buffer[(tx_buffer).bufferWritePointer] = byte;\
		(tx_buffer).bufferWritePointer = ((tx_buffer).bufferWritePointer + 1) & TX_BUFFER_MASK;\
		}

#if UART_PORT_DEFAULT == 0
#define LPC_UART LPC_USART0
#elif UART_PORT_DEFAULT == 1
#define LPC_UART ((LPC_USART_T*)LPC_UART1)
#else
#error "undefined value of UART"
#endif

void UARTInit(LPC_USART_T* UARTx, uint32_t Baudrate);
void UART0ParseNewChar(unsigned char newChar);
extern uint32_t freeSpaceFor(uint32_t nbytes);

#endif /* end __UART_H */
/*****************************************************************************
 **                            End Of File
 ******************************************************************************/
