/*
 * extra_pins.c
 *
 *  Created on: 06/04/2014
 *      Author: ruka
 */
#include "extra_pins.h"
#include "chip.h"

void ExtraPinsInit() {

	Chip_SCU_PinMuxSet(VBAT_GND_PORT, VBAT_GND_PIN, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, VBAT_GND_PORT_GPIO, VBAT_GND_PIN_GPIO); /* set P1.17 as output */
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, VBAT_GND_PORT_GPIO, VBAT_GND_PIN_GPIO); //drive to ground

	Chip_SCU_PinMuxSet(FTDI_RESET_PORT, FTDI_RESET_PIN, MD_PLN_FAST | FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, FTDI_RESET_PORT_GPIO, FTDI_RESET_PIN_GPIO); /* set P1.15 as output */
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, FTDI_RESET_PORT_GPIO, FTDI_RESET_PIN_GPIO); //enable the FTDI

	Chip_SCU_PinMuxSet(LED0_PORT, LED0_PIN, MD_PLN_FAST | FUNC0);
	// set P0.0 as output
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO); //Turn on the LED

	Chip_SCU_PinMuxSet(LED1_PORT, LED1_PIN, MD_PLN_FAST | FUNC0);
	// set P0.1 as output
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO); // Keep it Off
}

