/* Kernel includes. */

#include "chip.h"
#include "motors.h"
#include "sleep.h"
#include "sensors.h"
#include "uart.h"
#include "EDVS128_LPC43xx.h"
#include "mpu9105.h"
#include "ff.h"
#include "extra_pins.h"
#include "pwm.h"
#include "minirob.h"
#include "cr_start_m0.h"
//Uncomment the line below to activate test mode.
//#include "test.h"

#include <cr_section_macros.h>
#include <NXP/crp.h>

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP;

#define SD_ERROR 1

uint8_t fileBuffer[_MAX_SS * 4];				// events recording
uint32_t fileBufferIndex = 0;

FATFS fs;

int main(void) {
	uint32_t DVSEventTime;
	uint16_t DVSEvent;
	uint8_t isRecording = 0, hasSD = 0;
	char filename[19];
	UINT bytesWritten;
	FIL outputFile;
	DIR dir;
	//disable_peripherals();
	ExtraPinsInit();
	timer_delay_ms(100);
	LEDSetOff();
	RTCInit();
	DVS128ChipInit();
	DacInit();
	UARTInit(LPC_UART, BAUD_RATE_DEFAULT); /* baud rate setting */
	initMotors();
	MPU9105Init();
	PWMInit();
	sensorsInit();
	MiniRobInit();

#ifdef TEST_RUN
	test();
	//This will not return
#endif

	// Start M0APP slave processor
#if defined (LPC43_MULTICORE_M0APP)
	cr_start_m0(SLAVE_M0APP, &__core_m0app_START__);
#endif

	LEDSetOn();
	for (;;) {
		// *****************************************************************************
		//    UARTIterate();
		// *****************************************************************************
		while (LPC_UART->LSR & UART_LSR_RDR) {  // incoming char available?
			UART0ParseNewChar((uint8_t) LPC_UART->RBR);
		}

#if UART_PORT_DEFAULT == 0
		if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, CTS0_GPIO_PORT, CTS0_GPIO_PIN) == 0) { // no rts stop signal
#endif
			while ((tx_buffer.bufferReadPointer != tx_buffer.bufferWritePointer) && (LPC_USART0->LSR & UART_LSR_THRE)) {
				LPC_USART0->THR = tx_buffer.buffer[tx_buffer.bufferReadPointer];
				tx_buffer.bufferReadPointer = (tx_buffer.bufferReadPointer + 1) & TX_BUFFER_MASK;
			}
#if UART_PORT_DEFAULT == 0
		}
#endif

		if (tx_buffer.bufferReadPointer != tx_buffer.bufferWritePointer) {				// wait for TX to finish sending!
			continue;
		}
		updateIMUData();
		refreshMiniRobSensors();
		if (sensorRefreshRequested) {
			sensorRefreshRequested = 0;
			for (int i = 0; i < sensorsEnabledCounter; ++i) {
				if (enabledSensors[i]->triggered) {
					enabledSensors[i]->refresh();
					enabledSensors[i]->triggered = 0;
				}
			}
		}

		// *****************************************************************************
		//    processEventsIterate();
		// *****************************************************************************
		if (eventBufferWritePointer == eventBufferReadPointer) {		// more events in buffer to process?
			continue;
		}

		eventBufferReadPointer = ((eventBufferReadPointer + 1) & DVS_EVENTBUFFER_MASK);		 // increase read pointer
		DVSEvent = eventBufferA[eventBufferReadPointer];		 // fetch event from buffer
		DVSEventTime = eventBufferT[eventBufferReadPointer];	// fetch event from buffer
		if (enableEventSending) {
			if (eDVSDataFormat == EDVS_DATA_FORMAT_BIN) {
				PUSH_BYTE(tx_buffer, ((DVSEvent >> 8) & 0x7F) | 0x80); // 1st byte to send (Y-address)
				PUSH_BYTE(tx_buffer, DVSEvent & 0xFF);	// 2nd byte to send (X-address)
			} else {
				if (eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS2B) {
					PUSH_BYTE(tx_buffer, ((DVSEvent >> 8) & 0x7F) | 0x80); // 1st byte to send (Y-address)
					PUSH_BYTE(tx_buffer, DVSEvent & 0xFF);	// 2nd byte to send (X-address)
					PUSH_BYTE(tx_buffer, (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 8)) & 0xFF);	// 3rd byte to send (time stamp high byte)
					PUSH_BYTE(tx_buffer, (DVSEventTime >> (TIMESTAMP_SHIFTBITS)) & 0xFF);	// 4th byte to send (time stamp low byte)
				} else {
					PUSH_BYTE(tx_buffer, ((DVSEvent >> 8) & 0x7F) | 0x80); // 1st byte to send (Y-address)
					PUSH_BYTE(tx_buffer, DVSEvent & 0xFF);	// 2nd byte to send (X-address)
					PUSH_BYTE(tx_buffer, (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 16)) & 0xFF);	// 3rd byte to send (time stamp high byte)
					PUSH_BYTE(tx_buffer, (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 8)) & 0xFF);	// 4th byte to send (time stamp)
					PUSH_BYTE(tx_buffer, (DVSEventTime >> (TIMESTAMP_SHIFTBITS)) & 0xFF);	// 5th byte to send (time stamp low byte)
				}
			}
		}

		//Recording session in SD using the SPI slot
		if (shouldRecord == 1) {
			if (isRecording == 0) {
				if (hasSD != SD_ERROR) {
					f_mount(0, &fs); //always ok, doesn't actually talks to card
					if (f_opendir(&dir, "/") == FR_OK) {
						getFilename(filename, currentEventRate);
						if (f_open(&outputFile, filename,
						FA_WRITE | FA_CREATE_NEW | FA_OPEN_ALWAYS) == FR_OK) {
							isRecording = 1;
							if (eDVSDataFormat == EDVS_DATA_FORMAT_BIN) {
								fileBuffer[fileBufferIndex++] = (DVSEvent >> 8) & 0x7F;
								fileBuffer[fileBufferIndex++] = DVSEvent & 0xFF;
							} else {
								if (eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS2B) {
									fileBuffer[fileBufferIndex++] = (DVSEvent >> 8) & 0x7F;
									fileBuffer[fileBufferIndex++] = DVSEvent & 0xFF;
									fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 8)) & 0xFF;
									fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS)) & 0xFF;
								} else {
									fileBuffer[fileBufferIndex++] = (DVSEvent >> 8) & 0x7F;
									fileBuffer[fileBufferIndex++] = DVSEvent & 0xFF;
									fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 16)) & 0xFF;
									fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 8)) & 0xFF;
									fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS)) & 0xFF;
								}
							}
						} else
							hasSD = SD_ERROR;
					} else
						hasSD = SD_ERROR;
				}
			} else {
				if (eDVSDataFormat == EDVS_DATA_FORMAT_BIN) {
					fileBuffer[fileBufferIndex++] = (DVSEvent >> 8) & 0x7F;
					fileBuffer[fileBufferIndex++] = DVSEvent & 0xFF;
				} else {
					if (eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS2B) {
						fileBuffer[fileBufferIndex++] = (DVSEvent >> 8) & 0x7F;
						fileBuffer[fileBufferIndex++] = DVSEvent & 0xFF;
						fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 8)) & 0xFF;
						fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS)) & 0xFF;
					} else {
						fileBuffer[fileBufferIndex++] = (DVSEvent >> 8) & 0x7F;
						fileBuffer[fileBufferIndex++] = DVSEvent & 0xFF;
						fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 16)) & 0xFF;
						fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS + 8)) & 0xFF;
						fileBuffer[fileBufferIndex++] = (DVSEventTime >> (TIMESTAMP_SHIFTBITS)) & 0xFF;
					}
				}
				if (fileBufferIndex >= _MAX_SS) {
					f_write(&outputFile, fileBuffer, fileBufferIndex, &bytesWritten); //write data
					fileBufferIndex = 0;
				}
			}
		} else {
			hasSD = 0; //try again later
			f_mount(0, NULL); //unmounting the card
			if (isRecording == 1) {
				isRecording = 0;
				if (fileBufferIndex != 0) {
					f_write(&outputFile, fileBuffer, fileBufferIndex, &bytesWritten); //write data
					fileBufferIndex = 0;
				}
				f_close(&outputFile);
			}
		}
	}

}

#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t l) {

	while (1) {

	}
}
#endif
