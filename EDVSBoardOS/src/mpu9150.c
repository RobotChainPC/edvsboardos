/*
 * mpu9150.c
 *
 *  Created on: Apr 7, 2014
 *      Author: raraujo
 */
#include "chip.h"
#include "mpu9105.h"

#define RECEIVING_IMU				(10)
#define RECEIVED_IMU				(11)
#define RECEIVING_COMPASS			(20)
#define RECEIVED_COMPASS			(21)

// P3.1 VBAT reset signal
#define MPU_INT_GND_PORT_GPIO  		(5)
#define MPU_INT_GND_PIN_GPIO  		(8)
#define MPU_INT_GND_PORT  			(3)
#define MPU_INT_GND_PIN  			(1)

// MPU9250 configuration
#define MPU_I2C_ADDR				0x68
#define MPU_REG_WHO_AM_I			0x75
#define MPU_REG_PWR_MGMT_1			0x6B
#define MPU_CYCLE					0x20
#define MPU_40_HZ					0xC0
#define MPU_20_HZ					0x80
#define MPU_5_HZ					0x40
#define MPU_1_25_HZ					0x00
#define MPU_REG_INT_PIN_CFG			0x37
#define MPU_I2C_LATCH_INT			0x20
#define MPU_I2C_INT_ENABLE			0x01

// AK8975C configuration
#define MPU_MAG_I2C_ADD				0x0C
#define MPU_REG_MAG_WIA				0x00
#define MPU_REG_MAG_CNTL			0x0A
#define MPU_MAG_SINGLE_MRMNT		0x01

#define MPU_REG_MAG_ST1				0x02
#define MPU_MAG_ST1_NORMAL			0x00
#define MPU_MAG_ST1_READY			0x01

// magnetic measurement data
#define MPU_REG_MAG_X				0x03
#define MPU_REG_MAG_Y				0x05
#define MPU_REG_MAG_Z				0x07

#define MPU_REG_INT_STATUS			0x3A

// acceleration measurement data
#define MPU_REG_ACC_X				0x3B
#define MPU_REG_ACC_Y				0x3D
#define MPU_REG_ACC_Z				0x3F

// gyro measurement data
#define MPU_REG_GYR_X				0x43
#define MPU_REG_GYR_Y				0x45
#define MPU_REG_GYR_Z				0x47

#define SFSI2C0_CONFIGURE_STANDARD_FAST_MODE		(1<<3 | 1<<11)
#define FAST_MODE_BAUD ((uint32_t)400000)

volatile int16_t gyrometer_data[3];
volatile int16_t temperature;
volatile int16_t accelerometer_data[3];
volatile int16_t magnometer_data[3];
volatile int16_t dataReceivingStep = RECEIVING_IMU;

volatile uint8_t buffer[15];
static const uint8_t CMD_READ_COMPASS = MPU_REG_MAG_X;
static const uint8_t CMD_READ_IMU = MPU_REG_INT_STATUS;

I2C_XFER_T xfer = { 0 };

#define INTERRPUT_VERSION 0

void I2C0_IRQHandler(void) {
	Chip_I2C_MasterStateHandler(I2C0);
}
#if INTERRPUT_VERSION
void MPU_Read_EventHandler(I2C_ID_T id, I2C_EVENT_T event) {

	/* Only DONE event needs to be handled */
	if (event != I2C_EVENT_DONE) {
		return;
	}

	if (dataReceivingStep == RECEIVING_IMU) {
		gyrometer_data[0] = ((int16_t) buffer[1] << 8) | ((int16_t) buffer[2] & 0xFF);
		gyrometer_data[1] = ((int16_t) buffer[3] << 8) | ((int16_t) buffer[4] & 0xFF);
		gyrometer_data[2] = ((int16_t) buffer[5] << 8) | ((int16_t) buffer[6] & 0xFF);
		temperature = ((int16_t) buffer[7] << 8) | ((int16_t) buffer[8] & 0xFF);
		accelerometer_data[0] = ((int16_t) buffer[9] << 8) | ((int16_t) buffer[10] & 0xFF);
		accelerometer_data[1] = ((int16_t) buffer[11] << 8) | ((int16_t) buffer[12] & 0xFF);
		accelerometer_data[2] = ((int16_t) buffer[13] << 8) | ((int16_t) buffer[14] & 0xFF);
		dataReceivingStep = RECEIVED_IMU;
	} else if (dataReceivingStep == RECEIVING_COMPASS) {
		magnometer_data[0] = ((int16_t) buffer[0] << 8) | ((int16_t) buffer[1] & 0xFF);
		magnometer_data[1] = ((int16_t) buffer[2] << 8) | ((int16_t) buffer[3] & 0xFF);
		magnometer_data[2] = ((int16_t) buffer[4] << 8) | ((int16_t) buffer[5] & 0xFF);
		dataReceivingStep = RECEIVED_COMPASS;
	}
}
#endif

void updateIMUData() {
	if (Chip_GPIO_ReadValue(LPC_GPIO_PORT, MPU_INT_GND_PORT_GPIO) & _BIT(MPU_INT_GND_PIN_GPIO)) {
#if INTERRPUT_VERSION
		if (dataReceivingStep == RECEIVED_COMPASS) {
			dataReceivingStep = RECEIVING_IMU;
			xfer.txBuff = &CMD_READ_IMU;
			xfer.rxBuff = buffer;
			xfer.rxSz = 15;
			Chip_I2C_Start_MasterTransfer(I2C0, &xfer);
		}
#else
		buffer[0] = MPU_REG_MAG_CNTL;
		buffer[1] = MPU_MAG_SINGLE_MRMNT;
		Chip_I2C_MasterSend(I2C0, MPU_I2C_ADDR, buffer, 2);
		Chip_I2C_MasterCmdRead(I2C0, MPU_I2C_ADDR, MPU_REG_INT_STATUS, buffer, 15);
		gyrometer_data[0] = ((int16_t) buffer[1] << 8) | ((int16_t) buffer[2] & 0xFF);
		gyrometer_data[1] = ((int16_t) buffer[3] << 8) | ((int16_t) buffer[4] & 0xFF);
		gyrometer_data[2] = ((int16_t) buffer[5] << 8) | ((int16_t) buffer[6] & 0xFF);
		temperature = ((int16_t) buffer[7] << 8) | ((int16_t) buffer[8] & 0xFF);
		accelerometer_data[0] = ((int16_t) buffer[9] << 8) | ((int16_t) buffer[10] & 0xFF);
		accelerometer_data[1] = ((int16_t) buffer[11] << 8) | ((int16_t) buffer[12] & 0xFF);
		accelerometer_data[2] = ((int16_t) buffer[13] << 8) | ((int16_t) buffer[14] & 0xFF);
		Chip_I2C_MasterCmdRead(I2C0, MPU_I2C_ADDR, MPU_REG_MAG_X, buffer, 6);
		magnometer_data[0] = ((int16_t) buffer[0] << 8) | ((int16_t) buffer[1] & 0xFF);
		magnometer_data[1] = ((int16_t) buffer[2] << 8) | ((int16_t) buffer[3] & 0xFF);
		magnometer_data[2] = ((int16_t) buffer[4] << 8) | ((int16_t) buffer[5] & 0xFF);

#endif
	}

#if INTERRPUT_VERSION
	if (dataReceivingStep == RECEIVED_IMU) {
		dataReceivingStep = RECEIVING_COMPASS;
		xfer.txBuff = &CMD_READ_COMPASS;
		xfer.rxBuff = buffer;
		xfer.rxSz = 6;
		Chip_I2C_Start_MasterTransfer(I2C0, &xfer);
	}
#endif
}

void MPU9105Init() {
	uint8_t initialBuffer[14];
	Chip_SCU_PinMuxSet(MPU_INT_GND_PORT, MPU_INT_GND_PIN, SCU_MODE_INBUFF_EN | SCU_MODE_FUNC4 | SCU_MODE_PULLUP);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, MPU_INT_GND_PORT_GPIO, MPU_INT_GND_PIN_GPIO);

	Chip_I2C_Init(I2C0);
	Chip_I2C_SetClockRate(I2C0, FAST_MODE_BAUD);
	Chip_I2C_SetMasterEventHandler(I2C0, Chip_I2C_EventHandler);
	NVIC_EnableIRQ(I2C0_IRQn);
	LPC_SCU->SFSI2C0 = SFSI2C0_CONFIGURE_STANDARD_FAST_MODE;
	if (Chip_I2C_MasterCmdRead(I2C0, MPU_I2C_ADDR, MPU_REG_WHO_AM_I, initialBuffer, 1) < 1) {
		NVIC_DisableIRQ(I2C0_IRQn);
		Chip_I2C_DeInit(I2C0);
		return;

	} else {
		if (initialBuffer[0] != MPU_I2C_ADDR) {
			NVIC_DisableIRQ(I2C0_IRQn);
			Chip_I2C_DeInit(I2C0);
			return;
		}
	}
	//Wake up the sleepy IMU and set it to update the data at the max rate 40Hz
	initialBuffer[0] = MPU_REG_PWR_MGMT_1;
	initialBuffer[1] = MPU_CYCLE;
	initialBuffer[2] = MPU_40_HZ;
	Chip_I2C_MasterSend(I2C0, MPU_I2C_ADDR, initialBuffer, 3);

	// Config the Interrupt to latch and
	initialBuffer[0] = MPU_REG_INT_PIN_CFG;
	initialBuffer[1] = MPU_I2C_LATCH_INT;
	initialBuffer[2] = MPU_I2C_INT_ENABLE;
	Chip_I2C_MasterSend(I2C0, MPU_I2C_ADDR, initialBuffer, 3);

	//Engage the Magnetometer
	initialBuffer[0] = MPU_REG_MAG_CNTL;
	initialBuffer[1] = MPU_MAG_SINGLE_MRMNT;
	Chip_I2C_MasterSend(I2C0, MPU_I2C_ADDR, initialBuffer, 2);

	Chip_I2C_MasterCmdRead(I2C0, MPU_I2C_ADDR, MPU_REG_ACC_X, initialBuffer, sizeof(initialBuffer));
	gyrometer_data[0] = ((int16_t) initialBuffer[0] << 8) | ((int16_t) initialBuffer[1] & 0xFF);
	gyrometer_data[1] = ((int16_t) initialBuffer[2] << 8) | ((int16_t) initialBuffer[3] & 0xFF);
	gyrometer_data[2] = ((int16_t) initialBuffer[4] << 8) | ((int16_t) initialBuffer[5] & 0xFF);
	temperature = ((int16_t) initialBuffer[6] << 8) | ((int16_t) initialBuffer[7] & 0xFF);
	accelerometer_data[0] = ((int16_t) initialBuffer[8] << 8) | ((int16_t) initialBuffer[9] & 0xFF);
	accelerometer_data[1] = ((int16_t) initialBuffer[10] << 8) | ((int16_t) initialBuffer[11] & 0xFF);
	accelerometer_data[2] = ((int16_t) initialBuffer[12] << 8) | ((int16_t) initialBuffer[13] & 0xFF);
	Chip_I2C_MasterCmdRead(I2C0, MPU_I2C_ADDR, MPU_REG_MAG_X, initialBuffer, 6);
	magnometer_data[0] = ((int16_t) initialBuffer[0] << 8) | ((int16_t) initialBuffer[1] & 0xFF);
	magnometer_data[1] = ((int16_t) initialBuffer[2] << 8) | ((int16_t) initialBuffer[3] & 0xFF);
	magnometer_data[2] = ((int16_t) initialBuffer[4] << 8) | ((int16_t) initialBuffer[5] & 0xFF);

#if INTERRPUT_VERSION
	xfer.slaveAddr = MPU_I2C_ADDR;
	xfer.txSz = 1;
	dataReceivingStep = RECEIVED_COMPASS;

	Chip_I2C_SetMasterEventHandler(I2C0, MPU_Read_EventHandler);
#endif
}

