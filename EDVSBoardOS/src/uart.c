/***********************************************************************
 * $Id::                                                               $
 *
 * Project:	uart: Simple UART echo for LPCXpresso 1700
 * File:	uart.c
 * Description:
 * 			LPCXpresso Baseboard uses pins mapped to UART3 for
 * 			its USB-to-UART bridge. This application simply echos
 * 			all characters received.
 *
 ***********************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 **********************************************************************/

/*****************************************************************************
 *   History
 *   2010.07.01  ver 1.01    Added support for UART3, tested on LPCXpresso 1700
 *   2009.05.27  ver 1.00    Prelimnary version, first Release
 *
 ******************************************************************************/
#include <stdio.h>
#include "chip.h"

#include "config.h"
#include "extra_pins.h"
#include "uart.h"
#include "sensors.h"
#include "motors.h"
#include "sleep.h"
#include "pwm.h"
#include "EDVS128_LPC43xx.h"

#define MHZ								(1000000)
#define SOFTWARE_VERSION				"1.0"
#define UART_COMMAND_LINE_MAX_LENGTH  	96

struct transmist_buffer tx_buffer;
// *****************************************************************************

unsigned char commandLine[UART_COMMAND_LINE_MAX_LENGTH];
uint32_t commandLinePointer;
uint32_t enableUARTecho;	 										// 0-no cmd echo, 1-only cmd reply, 2-all visible

// *****************************************************************************
#define UARTReturn()	   putchar('\n')

/* The rate at which data is sent to the queue, specified in milliseconds. */

/*****************************************************************************
 ** Function name:		UARTInit
 **
 ** Descriptions:		Initialize UART port, setup pin select,
 **						clock, parity, stop bits, FIFO, etc.
 **
 ** parameters:			portNum(0 or 1) and UART baudrate
 ** Returned value:		true or false, return false only if the
 **						interrupt handler can't be installed to the
 **						VIC table
 **
 *****************************************************************************/
void UARTInit(LPC_USART_T* UARTx, uint32_t baudrate) {
	if (UARTx == LPC_USART0) {
		/* RxD0 is P2.1 and TxD0 is P2.0 */
		Chip_SCU_PinMuxSet(2, 0, MD_PLN_FAST | FUNC1);
		Chip_SCU_PinMuxSet(2, 1, MD_PLN_FAST | MD_EZI | FUNC1);
		Chip_SCU_PinMuxSet(RTS0_PORT, RTS0_PIN, MD_PLN_FAST | FUNC0);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, RTS0_GPIO_PORT, RTS0_GPIO_PIN); //Signal ready to the DTE
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, RTS0_GPIO_PORT, RTS0_GPIO_PIN);
		Chip_SCU_PinMuxSet( CTS0_PORT, CTS0_PIN, MD_BUK | MD_EZI | FUNC0);
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, CTS0_GPIO_PORT, CTS0_GPIO_PIN);
	} else if (UARTx == LPC_UART1) {
		/* Enable RTS1  P5.2, CTS1 P5.4, RxD1 P1.14, TxD1 P3.4*/
		Chip_SCU_PinMuxSet(3, 4, MD_PLN_FAST | FUNC4);
		Chip_SCU_PinMuxSet(1, 14, MD_BUK | MD_EZI | FUNC1);
		Chip_SCU_PinMuxSet(5, 2, MD_PLN_FAST | FUNC4);
		Chip_SCU_PinMuxSet(5, 4, MD_BUK | MD_EZI | FUNC4);
		Chip_UART_SetModemControl(LPC_UART1, UART_MCR_AUTO_RTS_EN | UART_MCR_AUTO_CTS_EN);
	}
	Chip_UART_Init(UARTx);
	Chip_UART_SetBaudFDR(UARTx, baudrate);
	Chip_UART_TXEnable(UARTx);

	commandLine[0] = 0;
	commandLinePointer = 0;
	enableUARTecho = 2;
}

uint32_t freeSpaceFor(uint32_t nbytes) {
	if (tx_buffer.bufferReadPointer == tx_buffer.bufferWritePointer)
		return nbytes < TX_BUFFER_SIZE ? 1 : 0;
	if (tx_buffer.bufferReadPointer > tx_buffer.bufferWritePointer) {
		return nbytes < (tx_buffer.bufferReadPointer - tx_buffer.bufferWritePointer) ? 1 : 0;
	} else {
		return nbytes < (TX_BUFFER_SIZE - tx_buffer.bufferWritePointer + tx_buffer.bufferReadPointer) ? 1 : 0;
	}

}

/*****************************************************************************
 ** Function name:		UARTSend
 **
 ** Descriptions:		Send a block of data to the UART 0 port based
 **						on the data length
 **
 ** parameters:			portNum, buffer pointer, and data length
 ** Returned value:		None
 **
 *****************************************************************************/

int __sys_write(int iFileHandle, char *pcBuffer, int iLength) {
	if (freeSpaceFor(iLength)) {
		for (uint32_t i = 0; i < iLength; ++i) {
			PUSH_BYTE(tx_buffer, *pcBuffer);
			pcBuffer++;
		}
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO); //Turn off the LED
	} else {
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO); //Turn off the LED
	}
	return iLength;
}

// *****************************************************************************

// *****************************************************************************
//TODO
/*void UART0SetBaudRate(uint32_t baudRate) {

 switch (baudRate) {
 case ((uint32_t) 19200):
 UART0_DLL = (0x7D);
 UART0_FDR = (((0x03) << 4) | 0x02);
 break;
 case ((uint32_t) 31250):
 UART0_DLL = (0x80);
 break;
 case ((uint32_t) 38400):
 UART0_DLL = (0x32);
 UART0_FDR = (((0x0C) << 4) | 0x0D);
 break;
 case ((uint32_t) 57600):
 UART0_DLL = (0x36);
 UART0_FDR = (((0x07) << 4) | 0x02);
 break;
 case ((uint32_t) 62500):
 UART0_DLL = (0x40);
 break;
 case ((uint32_t) 115200):
 UART0_DLL = (0x1B);
 UART0_FDR = (((0x07) << 4) | 0x02);
 break;
 case ((uint32_t) 125000):
 UART0_DLL = (0x20);
 break;
 case ((uint32_t) 230400):
 UART0_DLL = (0x09);
 UART0_FDR = (((0x0E) << 4) | 0x0D);
 break;
 case ((uint32_t) 250000):
 UART0_DLL = (0x10);
 break;
 case ((uint32_t) 460800):
 UART0_DLL = (0x08);
 UART0_FDR = (((0x0C) << 4) | 0x01);
 break;
 case ((uint32_t) 500000):
 UART0_DLL = (0x08);
 break;
 case ((uint32_t) 921600):
 UART0_DLL = (0x04);
 UART0_FDR = (((0x0C) << 4) | 0x01);
 break;
 case ((uint32_t) 1):
 case ((uint32_t) 1000000):
 UART0_DLL = (0x04);
 break;
 case ((uint32_t) 1500000):
 UART0_DLL = (0x02);
 UART0_FDR = (((0x03) << 4) | 0x01);
 break;
 case ((uint32_t) 1843200):
 UART0_DLL = (0x02);
 UART0_FDR = (((0x0C) << 4) | 0x01);
 break;
 case ((uint32_t) 2):
 case ((uint32_t) 2000000):
 UART0_DLL = (0x02);
 break;
 case ((uint32_t) 4):
 case ((uint32_t) 4000000):
 UART0_DLL = (0x01);
 break;

 default:
 UART0_LCR = 0x03;				// Close divisor before printing!
 printf("unknown/unsupported baud rate!\n");
 return;
 }

 UART0_LCR = 0x03;							// Close divisor
 }
 */
// *****************************************************************************
void UARTShowVersion(void) {
	UARTReturn();
	printf("EDVS-4337, V"); //Programming WORKS, BITCHES!
	printf(SOFTWARE_VERSION);
	printf(": ");
	printf(__DATE__);
	printf(", ");
	printf(__TIME__);
	UARTReturn();

	printf("System Clock: %2dMHz / 3*%d -> %dns event time resolution", SystemCoreClock / MHZ, (1 << TIMESTAMP_SHIFTBITS), (1000 * (1 << TIMESTAMP_SHIFTBITS)) / (SystemCoreClock / (3 * MHZ)));
	UARTReturn();
}

// *****************************************************************************
void UARTShowUsage(void) {

	UARTShowVersion();

	UARTReturn();
	printf("Supported Commands:\n");
	UARTReturn();

	printf(" E+/-          - enable/disable event sending\n");
	printf(" !Ex           - specify event data format, ?E to list options\n");
	UARTReturn();

	printf(" !Bx=y         - set bias register x[0..11] to value y[0..0xFFFFFF]\n");
	printf(" !BF           - send bias settings to DVS\n");
	printf(" !BDx          - select and flush default bias set (default: set 0)\n");
	printf(" ?Bx           - get bias register x current value\n");
	UARTReturn();

	printf(" 0,1           - LED off/on\n");
	//printf(" !U=x       	- set baudrate to x\n"); //TODO
	printf(" !U[0,1,2]     - UART echo mode (none, cmd-reply, all)\n");
	UARTReturn();

	printf(" !Sn,b,p       - enable/disable sensors streaming, ?S for details\n");
	//printf(" ?Sb  			- get sensor readouts according to the bitmap b, ?S for details\n"); //TODO
	UARTReturn();

	printf(" R             - reset board\n");
	//printf(" S             - enter sleep mode\n");
	printf(" P             - enter reprogramming mode\n");
	UARTReturn();

	printf(" M+/-          - enable/disable motor driver\n");
	printf(" !Mx=y         - set motor x to duty cycle y\n");
	printf(" !MPx=y        - set motor x PWM period to y\n");
	printf(" !MVx=y        - set motor x velocity to y, it decays with in 1 second\n");
	UARTReturn();

	printf(" !P[A,B,C]=x   - set Timer [A,B,C] to period x\n");
	printf(" !P[A,B,C]x=y  - set Timer [A,B,C] channel x with duty cycle y\n");
	UARTReturn();

	//printf(" ?T      		- get RTC time\n"); //TODO
	UARTReturn();

	printf(" ??            - display help\n");
	UARTReturn();
}

void UARTShowEventDataOptions(void) {
	printf(" !E0   - 2 bytes per event binary 0yyyyyyy.pxxxxxxx (default)\n");
	printf(" !E1   - 4 bytes per event (as above followed by 16bit timestamp 1us res)\n");
	printf(" !E2   - 5 bytes per event (as above followed by 24bit timestamp 1us res)\n");
	UARTReturn();
}

// *****************************************************************************
static uint32_t parseULong(unsigned char **c) {
	uint32_t ul = 0;
	while (((**c) >= '0') && ((**c) <= '9')) {
		ul = 10 * ul;
		ul += ((**c) - '0');
		(*(c))++;
	}
	return (ul);
}

static int32_t parseLong(unsigned char **c) {
	if ((**c) == '-') {
		(*(c))++;
		return (-1 * ((int32_t) parseULong(c)));
	}
	if ((**c) == '+') {
		(*(c))++;
	}
	return ((int32_t) parseULong(c));
}

// *****************************************************************************
// * ** parseGetCommand ** */
// *****************************************************************************
void UARTParseGetCommand(void) {

	switch (commandLine[1]) {

	case 'B':
	case 'b': {	   									// request bias value
		unsigned char *c;
		int32_t biasID;

		c = commandLine + 2;					// send bias value as deciman value
		if ((*c == 'A') || (*c == 'a')) {
			for (biasID = 0; biasID < 12; biasID++) {
				printf("-B%d=%d\n", biasID, DVS128BiasGet(biasID));
			}
			break;
		}

		biasID = parseULong(&c);
		printf("-B%d=%d\n", biasID, DVS128BiasGet(biasID));
		break;
	}

	case 'E':
	case 'e':
		printf("-E%d\n", eDVSDataFormat);
		break;

	case '?':
		if (((commandLine[2]) == 'e') || ((commandLine[2]) == 'E')) {
			UARTShowEventDataOptions();
			break;
		}
		UARTShowUsage();
		break;

	default:
		printf("Get: parsing error\n");
	}
	return;
}

// *****************************************************************************
// * ** parseSetCommand ** */
// *****************************************************************************
void UARTParseSetCommand(void) {
	switch (commandLine[1]) {
	case 'B':
	case 'b': {
		unsigned char *c;
		long biasID, biasValue;

		if ((commandLine[2] == 'F') || (commandLine[2] == 'f')) {	   	// flush bias values to DVS chip
			if ((enableEventSending == 0) && (enableUARTecho > 1)) {
				printf("-BF\n");
			}
			DVS128BiasFlush(1);
			break;
		}

		if ((commandLine[2] == 'D') || (commandLine[2] == 'd')) {	   	// load and flush default bias set
			if ((commandLine[3] > '0') && (commandLine[3] < '9')) {
				if ((enableEventSending == 0) && (enableUARTecho > 1)) {
					printf("-BD%c\n", commandLine[3]);
				}
				DVS128BiasLoadDefaultSet(commandLine[3] - '0');
				DVS128BiasFlush(1);
			} else {
				printf("Select default bias set: parsing error\n");
			}
			break;
		}

		c = commandLine + 2;
		biasID = parseULong(&c);
		c++;
		biasValue = parseULong(&c);
		DVS128BiasSet(biasID, biasValue);
		if ((enableEventSending == 0) && (enableUARTecho > 1)) {
			printf("-B%d=%d\n", biasID, DVS128BiasGet(biasID));
		}
		break;
	}

	case 'E':
	case 'e': {
		unsigned char *c = commandLine + 2;
		if ((*c) == '=')
			c++;   		   		// skip '=' if entered
		eDVSDataFormat = parseULong(&c);
		if ((enableEventSending == 0) && (enableUARTecho > 1)) {
			printf("-E%d\n", eDVSDataFormat);
		}
		break;
	}

	case 'M':
	case 'm': {
		unsigned char *c = commandLine + 2;
		uint32_t motorId = 0;
		if ((*c == 'V') || (*c == 'v')) {
			c++;
			motorId = parseULong(&c);
			c++;
			updateMotorVelocity(motorId, parseLong(&c));
			break;
		}
		if ((*c == 'P') || (*c == 'p')) {
			c++;
			motorId = parseULong(&c);
			c++;
			updatePWMPeriod(motorId, parseULong(&c));
			break;
		}
		motorId = parseULong(&c);
		c++;
		updateMotorDutyCycle(motorId, parseLong(&c));
		break;
	}

	case 'P':
	case 'p': {
		unsigned char *c = commandLine + 2;
		if ((*c >= 'A') && (*c <= 'C')) {
			uint8_t channel = *c - 'A';
			c++;
			if ((*c == '0') || (*c == '1')) {
				uint8_t output = *c - '0';
				c += 2;
				PWMSetDutyCycle(channel, output, parseULong(&c));
			} else {
				c++;
				PWMSetFrequency(channel, parseULong(&c));
			}
			break;
		} else {
			printf("Channel not recognized\n");
		}
		break;
	}

	case 'S':
	case 's': {
		unsigned char *c = commandLine + 2;
		uint8_t flag = *c == '+' ? ENABLE : DISABLE;
		c += 2;
		uint32_t mask = parseULong(&c), period = 0;
		c++;
		if (flag) {
			period = parseULong(&c);
		}
		enableSensors(mask, flag, period);
		break;
	}

	case 'U':
	case 'u': {
		unsigned char *c;
		long baudRate;
		c = commandLine + 2;
		if (((*c) >= '0') && ((*c) <= '2')) {
			enableUARTecho = ((*c) - '0');
			break;
		}
		c++;
		baudRate = parseULong(&c);
		if ((enableEventSending == 0) && (enableUARTecho > 1)) {
			printf("Switching Baud Rate to %d Baud!\n", baudRate);
		}
		while ((LPC_UART->LSR & UART_LSR_TEMT) == 0) {
		};		   // wait for UART to finish data transfer
		//UART0SetBaudRate(baudRate);
		break;
	}

	default:
		printf("Set: parsing error\n");
	}
	return;
}

// *****************************************************************************
// * ** parseRS232CommandLine ** */
// *****************************************************************************
void parseRS232CommandLine(void) {

	switch (commandLine[0]) {
	case '?':
		UARTParseGetCommand();
		break;
	case '!':
		UARTParseSetCommand();
		break;

	case 'P':
	case 'p':
		UARTInit(LPC_USART0, 9600);
		enterReprogrammingMode();
		break;
	case 'R':
	case 'r':
		resetDevice();
		break;

	case '0':
		LEDSetOff();
		break;
	case '1':
		LEDSetOn();
		break;

	case 'E':
	case 'e':
		if (commandLine[1] == '+') {
			DVS128FetchEventsEnable(TRUE);
		} else {
			DVS128FetchEventsEnable(FALSE);
		}
		break;

	case 'M':
	case 'm':
		if (commandLine[1] == '+') {
			enableMotorDriver(TRUE);
		} else {
			enableMotorDriver(FALSE);
		}
		break;

	case 'S':
	case 's': {
		//sleep();
//		long n;
//		long e;
//		printf("-tStart-\n");
//		for (n = 0; n < 61; n++) {			// send 1'998'848 events of 2 bytes each (= 3'997'696 bytes = 10sec @ 4mbit)
//			for (e = 0; e < 0x8000; e++) {
//
//#if UART_PORT_DEFAULT == 0
//				if ((LPC_GPIO_PORT->PIN[CTS0_GPIO_PORT] & _BIT(CTS0_GPIO_PIN)) == 0) { // no rts stop signal
//#endif
//					while (LPC_UART->LSR & UART_LSR_THRE) {
//						LPC_UART->THR = (e) & 0xFF;
//					}
//#if UART_PORT_DEFAULT == 0
//				}
//#endif
//			}
//		}
//		printf("-tStop-\n");
	}
		break;

	default:
		printf("?\n\r");
	}
	return;
}

// *****************************************************************************
// * ** RS232ParseNewChar ** */
// *****************************************************************************
void UART0ParseNewChar(unsigned char newChar) {

	switch (newChar) {
	case 8:			// backspace
		if (commandLinePointer > 0) {
			commandLinePointer--;
			if ((enableEventSending == 0) && (enableUARTecho)) {
				printf("%c %c", 8, 8);
			}
		}
		break;

	case 10:
	case 13:
		if ((enableEventSending == 0) && (enableUARTecho)) {
			UARTReturn();
		}
		if (commandLinePointer > 0) {
			commandLine[commandLinePointer] = 0;
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, RTS0_GPIO_PORT, RTS0_GPIO_PIN); //Signal ready to the DTE
			parseRS232CommandLine();
			Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, RTS0_GPIO_PORT, RTS0_GPIO_PIN); //Signal ready to the DTE
			commandLinePointer = 0;
		}
		break;

	default:
		if (commandLinePointer < (UART_COMMAND_LINE_MAX_LENGTH - 2)) {
			if ((enableEventSending == 0) && (enableUARTecho)) {
				putchar(newChar);	  		   	// echo to indicate char arrived
			}
			commandLine[commandLinePointer] = newChar;
			commandLinePointer++;
		} else {
			long n;
			printf("Reached cmd line length, resetting into bootloader mode!\n");
			for (n = 0; n < 100; n++) {
				timer_delay_ms(20);
				if (LPC_UART->LSR & UART_LSR_RDR) {				   // char arrived?
					newChar = LPC_UART->RBR;
				}
			}
			enterReprogrammingMode();
		}
	}  // end of switch

}  // end of rs232ParseNewChar

