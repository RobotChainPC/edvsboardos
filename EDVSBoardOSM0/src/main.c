/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */
#include "chip.h"
#include <cr_section_macros.h>
#define TIMER_CAPTURE_CHANNEL 		(1)
#define EVENT_PORT					(3)
#define PIN_ALL_ADDR            ((uint32_t) 0x00007FFF)// all 15 address bits from DVS#define DVS_EVENTBUFFER_SIZE_BITS		((uint32_t) 12)#define DVS_EVENTBUFFER_SIZE		  	(((uint32_t) 1)<<DVS_EVENTBUFFER_SIZE_BITS)#define DVS_EVENTBUFFER_MASK		  	(DVS_EVENTBUFFER_SIZE - 1)__DATA(RAM2) volatile uint16_t eventBufferA[DVS_EVENTBUFFER_SIZE]; // for event addresses__DATA(RAM3) volatile uint32_t eventBufferT[DVS_EVENTBUFFER_SIZE]; // for time stamps
__DATA(RAM7) volatile uint32_t eventBufferWritePointer = 0;
__DATA(RAM8) volatile uint32_t eventBufferReadPointer = 0;
__DATA(RAM9) volatile uint32_t currentEventRate = 0;

int main(void) {
	uint32_t DVSEventTime, DVSEventTimeOld;
	uint16_t DVSEvent;
	DVSEventTime = DVSEventTimeOld = Chip_TIMER_ReadCapture(LPC_TIMER1,TIMER_CAPTURE_CHANNEL);
	while (1) {

		DVSEventTime = Chip_TIMER_ReadCapture(LPC_TIMER1,TIMER_CAPTURE_CHANNEL);
		if (DVSEventTime != DVSEventTimeOld) {
			DVSEvent = Chip_GPIO_GetPortValue(LPC_GPIO_PORT,EVENT_PORT) & PIN_ALL_ADDR;
			currentEventRate++;
			eventBufferWritePointer = ((eventBufferWritePointer + 1)
					& DVS_EVENTBUFFER_MASK);
			if (eventBufferWritePointer == eventBufferReadPointer) {
				eventBufferReadPointer = ((eventBufferReadPointer + 1)
						& DVS_EVENTBUFFER_MASK);
			}
			eventBufferA[eventBufferWritePointer] = DVSEvent; // store event
			eventBufferT[eventBufferWritePointer] = DVSEventTime; // store event time
			DVSEventTimeOld = DVSEventTime;
		}
#if 0
		if ( LPC_TIMER0->TC < timersPWM[0]) {
			LPC_TIMER0->EMR |= 0x09;
		}
		timersPWM[0] = LPC_TIMER0->TC;

		if ( LPC_TIMER2->TC < timersPWM[1]) {
			LPC_TIMER2->EMR |= 0x03;
		}
		timersPWM[1] = LPC_TIMER2->TC;

		if ( LPC_TIMER3->TC < timersPWM[2]) {
			LPC_TIMER3->EMR |= 0x03;
		}
		timersPWM[2] = LPC_TIMER3->TC;
#endif
	}
	return 0;
}
